package PDB15_projekt;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import oracle.ord.im.OrdImage;
import oracle.spatial.geometry.JGeometry;


/**
 * GUI
 * @author Roman T�bi
 *
 */
public class GUI extends JPanel implements ItemListener, ActionListener, MouseListener, ComponentListener {
	
	private static final long serialVersionUID = 1L;
	
	private static final int TA_ROWS = 1;
	private static final int TA_COLS = 20;
	private static final int SEARCH_FIELD_WIDTH = 120;
	private static final int SEARCH_FIELD_HEIGHT = 25;
	private static final int SCROLL_LIST_FIELD_WIDTH = 140;
	private static final int DETAIL_IMAGE_WIDTH = 140;
	private static final int DETAIL_IMAGE_HEIGHT = 140;
	private static final int DETAIL_TEXTFIELD_WIDTH = 120;
	private static final int DETAIL_TEXTFIELD_HEIGHT = 25;
	private static final int DETAIL_CALENDAR_HEIGHT = 25;
	private static final int DETAIL_CALENDAR_VERTICAL_HEIGHT = 90;
	private static final int DETAIL_CALENDAR_WIDTH = 320;
	private static final int DETAIL_POPULATION_TIMELINE_WIDTH = 320;
	private static final int DETAIL_POPULATION_TIMELINE_HEIGHT = 50;
	private static final int DETAIL_POPULATION_BUTTON_WIDTH = 100;
	private static final int DETAIL_ROTATE_BUTTON_WIDTH = 100;
	
	private static final float ROTATION_ANGLE = 90.0f;
		
	private enum States {IDLE, ADD, EDIT};
	
	private States state = States.IDLE;

	private List<Animal> list;
	
	/**
	 * Search panel
	 */
	private JTextField tGenus;
	private JTextField tSpecies;
	private JTextField tPopulationBelow;
	private JTextField tPopulationAbove;
	
	private Calendar calPopulationAt;
	
	private JCheckBox tLawProtection;
	
	private JComboBox<String> tOrder;
	private final String[] orderStrings = { "", "bylinozravec", "vsezravec", "masozravec"};
	
	private JCheckBox cPopulationAt;
	private JCheckBox cGenus;
	private JCheckBox cSpecies;
	private JCheckBox cOrder;
	private JCheckBox cLawProtection;
	private JCheckBox cPopulationBelow;
	private JCheckBox cPopulationAbove;
	private JCheckBox cImage;
	private JCheckBox cAreaIntersection;
	private JCheckBox cBiggestArea;
	
	private JButton bImage;
	private JButton bSearch;
	
	private DefaultListModel<Object> listModel;
	private JList<Object> animalsList;
	private JScrollPane sScrollList;
	
	private JPanel pImagePreview;
	private JLabel lImagePreview;
	
	private	File searchFile = null;
	
	private JLabel lAnimalCount;

	/**
	 * Control panel
	 */
	private JButton bLogout;
	private JButton bEdit;
	private JButton bAdd;
	private JButton bDelete;
	private JButton bSaveChanges;
	private JButton bGetData;
	
	private JLabel lStatus = new JLabel(" ");
	
	/**
	 * Detail panel
	 */
	private JLabel lDGenus;
	private JLabel lDSpecies;
	private JLabel lDOrder;
	private JLabel lDPopulation;
	private JLabel lDPopulationFrom;
	private JLabel lDPopulationTo;		
	private JLabel lDDescribtion;	
	private JLabel lDLawProtection;
	
	private JButton bDDeletePopulation;
	private JButton bDAddPopulation;
	
	private Animal aDNewAnimal;
	
	private JTextField tDGenus;
	private JTextField tDSpecies;
	private JTextField tDPopulation;
	
	private DefaultListModel<Object> listModelTimeline;
	private JList listDPopulationTimeline;
	
	private JComboBox<String> tDOrder;
	
	private JCheckBox cDLawProtection;
	
	private Calendar calDPopulationFrom;
	private Calendar calDPopulationTo;
	
	private JTextArea tDDescribtion;
	
	private JPanel tDImagePreview;
	private JLabel lDImagePreview;
	
	private File newAnimalIcon = null;
	
	private JButton bDRotateImage;

	private Animal selectedAnimal;
	private int selectedAnimalIndexInList = -1;
	
	private boolean populationModifyFlag = false;
	private boolean rotationModifyFlag = false;
	/**
	 * Map containing information of searching animal
	 */
	private Map<String, String> animalSearchInfo = new HashMap<String, String>();

	/**
	 * Map panel
	 */
	private JPanel pMapPanel;
	private MapLabel lMapContainer;
	
	private JLabel lAreaSize;
	
	private int offset_X = 0;
	private int offset_Y = 0;
	
	private JComboBox shapeTypesComboBox;
	private String[] shapeTypesStrings = {"POLYGON", "MULTIPOLYGON", "POINT", "MULTIPOINT", "CURVE",
											"MULTICURVE"};
	/**
	 * Database
	 */
	private Database db;
	
	/**
	 * GUI constructor. Creates main panel with GridBag layout and all subpanels
	 * @param db
	 */
	public GUI(Database db) {		
		
		this.db = db;
		this.addComponentListener(this);
		// Cierna farba pre combobox ak je disabled
		UIManager.put("ComboBox.disabledForeground", Color.black);
		
		setLayout(new GridBagLayout());
		
		lAnimalCount = new JLabel(" ");
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 0;
		gbc.weighty = 0.8;
		add(createSearchPanel(), gbc);	
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.weightx = 0;
		gbc.weighty = 0;
		add(createControlPanel(), gbc);	
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		add(lAnimalCount, gbc);	
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		add(new JPanel(), gbc);	
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.weightx = 0;
		gbc.weighty = 0;
		add(new JPanel(), gbc);	

		gbc.weightx = 1;
		gbc.weighty = 0.6;
		gbc.gridx = 2;
		gbc.gridy = 0;
		add(createMapPanel(), gbc);	
		

		gbc.weightx = 1;
		gbc.weighty = 0;
		gbc.gridx = 2;
		gbc.gridy = 1;
		add(createEditShapeTypePanel(), gbc);	
		
		gbc.weightx = 1;
		gbc.weighty = 0.6;
		gbc.gridx = 2;
		gbc.gridy = 2;
		add(createAnimalDetailPanel(), gbc);	
		
	}	
	
	/**
	 * Create panel containing Map of Slovakia
	 * @return	map panel
	 */
	private Component createMapPanel() {		
		pMapPanel = new JPanel();
		pMapPanel.setLayout(new BorderLayout());

		lMapContainer = new MapLabel();
		

		lAreaSize = new JLabel();
		
		pMapPanel.add(lMapContainer, BorderLayout.CENTER);


		return pMapPanel;
	}
	
	/**
	 * Create panel edit type of shapes
	 * @return panel
	 */
	private JPanel createEditShapeTypePanel() {
		JPanel p = new JPanel();
		p.setLayout(new FlowLayout());
				
		shapeTypesComboBox = new JComboBox(shapeTypesStrings);
		shapeTypesComboBox.addActionListener(this);

		p.add(shapeTypesComboBox);
		p.add(lAreaSize);
		
		return p;
	}
	
	/**
	 * Load image from local repo and convert to Image Icon
	 * @return	image icon
	 */
	private ImageIcon loadIconFromFile(String fileName) {
		Dimension scaledIconDimension;
		BufferedImage file;
		Image image, scaledImg;
		ImageIcon scaledIcon;			
		
		image = null;
		scaledIcon = null;
		
		try {
	        

			file = ImageIO.read(Main.class.getResource(fileName));

		    image = (Image) file;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		scaledIconDimension = getScaledDimension	(	
														new Dimension(	image.getWidth(null), 
																		image.getHeight(null)),
														new Dimension(	pMapPanel.getWidth(), 
																		pMapPanel.getHeight())
													);
		
		scaledImg = image.getScaledInstance(	scaledIconDimension.width,
												scaledIconDimension.height, 
												java.awt.Image.SCALE_SMOOTH 
											);
		
		
		scaledIcon = new ImageIcon( scaledImg );
		
		return scaledIcon;
	}
	
	/**
	 * Initialization of Label containing picture of Slovakia
	 */
	public void initMap() {
		ImageIcon mapIcon;
		
//		String directory = (new File(this.getClass().getResource("Main.class").toString().substring(5))).getParent();
//        directory = directory.substring(0, directory.length() - 18);
        
 
		mapIcon = loadIconFromFile("/slovakia.jpg");
		
		if(mapIcon != null) {
			lMapContainer.setIcon(mapIcon);
		}
		
		offset_X = (pMapPanel.getWidth() - mapIcon.getIconWidth()) / 2;
		offset_Y = (pMapPanel.getHeight() - mapIcon.getIconHeight()) / 2;
				
	}
	/**
	 * Create search panel
	 * @return	search panel
	 */
	private JComponent createSearchPanel() {
		
		tGenus = new JTextField();
		tSpecies = new JTextField();
		tLawProtection = new JCheckBox();
		tPopulationBelow = new JTextField();
		tPopulationAbove = new JTextField();
		
		calPopulationAt = new Calendar(Calendar.VERTICAL);
		calPopulationAt.setEnabled(false);
		
		tOrder = new JComboBox<String>(orderStrings);
		tOrder.setSelectedIndex(0);
		tOrder.setEditable(false);
		
		tGenus.setEnabled(false);
		tSpecies.setEnabled(false);
		tOrder.setEnabled(false);
		tLawProtection.setEnabled(false);
		tPopulationBelow.setEnabled(false);
		tPopulationAbove.setEnabled(false);
		
		tGenus.setMaximumSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
		tSpecies.setMaximumSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
		tOrder.setMaximumSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
		tPopulationBelow.setMaximumSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
		tPopulationAbove.setMaximumSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
		
		cGenus = new JCheckBox("Genus");
		cSpecies = new JCheckBox("Species");
		cOrder = new JCheckBox("Order");
		cLawProtection = new JCheckBox("Law protection");
		cPopulationBelow = new JCheckBox("Population below");
		cPopulationAbove = new JCheckBox("Population above");
		cPopulationAt = new JCheckBox("Population at");
		cImage = new JCheckBox("Image");
		cAreaIntersection = new JCheckBox("Share same area");
		cBiggestArea = new JCheckBox("Biggest Area");
		
		cGenus.addItemListener(this);
		cSpecies.addItemListener(this);
		cOrder.addItemListener(this);
		cLawProtection.addItemListener(this);
		cPopulationBelow.addItemListener(this);
		cPopulationAbove.addItemListener(this);
		cPopulationAt.addItemListener(this);
		cImage.addItemListener(this);
		cAreaIntersection.addItemListener(this);
		cBiggestArea.addItemListener(this);

		bImage = new JButton("O");
		bImage.addActionListener(this);
		JPanel pImage = new JPanel();
		pImage.setLayout(new BorderLayout());
		pImage.add(bImage);
		pImage.setMaximumSize(pImage.getPreferredSize());
		bImage.setEnabled(false);
		
		bSearch = new JButton("Search");
		bSearch.addActionListener(this);
		JPanel pSearch = new JPanel();
		pSearch.setLayout(new BorderLayout());
		pSearch.add(bSearch);
		pSearch.setMaximumSize(pSearch.getPreferredSize());
		
		pImagePreview = new JPanel();
		pImagePreview.setMaximumSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_WIDTH));
		pImagePreview.setMinimumSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_WIDTH));
		pImagePreview.setBackground(Color.black);
		pImagePreview.setLayout(new BorderLayout());
		
		lImagePreview = new JLabel();
		
		pImagePreview.add(lImagePreview);
		
		
		JPanel panel = new JPanel();
		
		GroupLayout layout = new GroupLayout(panel);
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		panel.setLayout(layout);
		
		animalsList = new JList<Object>();	

		animalsList.setCellRenderer(new CellRenderer());
		animalsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		animalsList.setLayoutOrientation(JList.VERTICAL);
		
		animalsList.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent event) {
            	if (!event.getValueIsAdjusting()) {            		
            		
            		selectedAnimal = (Animal)animalsList.getSelectedValue();
            		selectedAnimalIndexInList = animalsList.getSelectedIndex();
            		            		
            		state = States.IDLE;
            		enableDetailPanel(false);
            		
            		if(selectedAnimalIndexInList == -1 && populationModifyFlag == false && rotationModifyFlag == false) {
            			clearDetailPanel();
            		}
            		else {
            			if(populationModifyFlag == false && rotationModifyFlag == false) {
                			clearDetailPanel();
            				displayAnimalDetail();
            				try {
            					lMapContainer.clear();
            					List<ShapeCoords> s = db.getAnimalHabitat(selectedAnimal.getId());
								lMapContainer.setShapes(s);
								
								Color c;
								
								Random random = new Random();
								int i = random.nextInt(5);
								
								switch(i){
								case 0:
									c = new Color(1.0f, 0.0f, 0.0f, 0.5f);
									break;
								case 1:
									c = new Color(0.0f, 204f/255f, 0.0f, 0.5f);
									break;
								case 2:
									c = new Color(0.0f, 0.0f, 1.0f, 0.5f);
									break;
								case 3:
									c = new Color(204f / 255f, 204f / 255f, 0.0f, 0.5f);
									break;
								case 4:
									c = new Color(204.0f / 255.0f, 0, 153f / 255f, 0.5f);
									break;
								case 5:
									c = new Color(1.0f, 0.5f, 0.0f, 0.5f);
									break;
								default:
									c = Color.BLACK;
										break;
								}
								
								lMapContainer.setColor(c);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}            				
            			}
            			else if(populationModifyFlag == true && rotationModifyFlag == false){
            				populationModifyFlag = false;
            				if(selectedAnimal != null) {
            					listPopulationTimeline(selectedAnimal);
            				}
            			}            			
            			else if(populationModifyFlag == false && rotationModifyFlag == true) {
            				rotationModifyFlag = false;
            				if(selectedAnimal != null) {
            					refreshDetailImage();
            				}
            			}
            		}
            	}
            }
        });		
		
		sScrollList = new JScrollPane(animalsList);
		sScrollList.setMinimumSize(new Dimension(SCROLL_LIST_FIELD_WIDTH, -1));

		layout.setHorizontalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
					.addComponent(cGenus)
					.addComponent(cSpecies)
					.addComponent(cOrder)
					.addComponent(cPopulationAbove)
					.addComponent(cPopulationBelow)
					.addComponent(cPopulationAt)
					.addComponent(cLawProtection)
					.addGroup(layout.createSequentialGroup()
							.addComponent(cImage)
							.addComponent(bImage))
					.addComponent(cAreaIntersection)
					.addComponent(cBiggestArea))
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup()
					.addComponent(tGenus)
					.addComponent(tSpecies)
					.addComponent(tOrder)
					.addComponent(tLawProtection)
					.addComponent(tPopulationBelow)
					.addComponent(tPopulationAbove)
					.addComponent(calPopulationAt)
					.addComponent(pImagePreview)
					.addComponent(bSearch))
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
				.addComponent(sScrollList));

		layout.setVerticalGroup(layout.createParallelGroup()
				.addGroup(layout.createSequentialGroup()
					.addGroup(layout.createParallelGroup()
							.addComponent(cGenus)
							.addComponent(tGenus))				
					.addGroup(layout.createParallelGroup()
							.addComponent(cSpecies)
							.addComponent(tSpecies))
					.addGroup(layout.createParallelGroup()
							.addComponent(cOrder)
							.addComponent(tOrder))
					.addGroup(layout.createParallelGroup()
							.addComponent(cPopulationAbove)
							.addComponent(tPopulationAbove))
					.addGroup(layout.createParallelGroup()
							.addComponent(cPopulationBelow)
							.addComponent(tPopulationBelow))
					.addGroup(layout.createParallelGroup()
							.addComponent(cPopulationAt)
							.addComponent(calPopulationAt, Alignment.LEADING, DETAIL_CALENDAR_VERTICAL_HEIGHT, DETAIL_CALENDAR_VERTICAL_HEIGHT, DETAIL_CALENDAR_VERTICAL_HEIGHT))
					.addGroup(layout.createParallelGroup()
							.addComponent(cLawProtection)
							.addComponent(tLawProtection))
					.addGroup(layout.createParallelGroup()
							.addGroup(layout.createParallelGroup()
								.addComponent(cImage)
								.addComponent(bImage))
							.addComponent(pImagePreview))
					.addComponent(cAreaIntersection)
					.addComponent(cBiggestArea)
					.addComponent(bSearch))
				.addComponent(sScrollList));	
		
		panel.setBorder(BorderFactory.createTitledBorder("Search"));
		
		return panel;
	}
	
	/**
	 * Create control panel
	 * @return	control panel
	 */
	private JComponent createControlPanel() {
		
		bLogout = new JButton("Logout");
		bEdit = new JButton("Edit");
		bAdd = new JButton("Add");
		bDelete = new JButton("Delete");
		bSaveChanges = new JButton("Save changes");
		bGetData = new JButton("Retrieve data");
		
		bLogout.addActionListener(this);
		bEdit.addActionListener(this);
		bAdd.addActionListener(this);
		bDelete.addActionListener(this);
		bSaveChanges.addActionListener(this);
		bGetData.addActionListener(this);
				
		JPanel pEdit = new JPanel();
		pEdit.setLayout(new BorderLayout());
		pEdit.add(bEdit);
		pEdit.setMaximumSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
		pEdit.setPreferredSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));

		JPanel pAdd = new JPanel();
		pAdd.setLayout(new BorderLayout());
		pAdd.add(bAdd);
		pAdd.setMaximumSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
		pAdd.setPreferredSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));

		
		JPanel pDelete = new JPanel();
		pDelete.setLayout(new BorderLayout());
		pDelete.add(bDelete);
		pDelete.setMaximumSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
		pDelete.setPreferredSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));

		
		JPanel pSaveChanges = new JPanel();
		pSaveChanges.setLayout(new BorderLayout());
		pSaveChanges.add(bSaveChanges);
		pSaveChanges.setMaximumSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
		pSaveChanges.setMinimumSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
		pSaveChanges.setPreferredSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));

		JPanel pUndoChanges = new JPanel();
		pUndoChanges.setLayout(new BorderLayout());
		pUndoChanges.add(bGetData);
		pUndoChanges.setMaximumSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
		pUndoChanges.setPreferredSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
		pUndoChanges.setMinimumSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));

		JPanel panel = new JPanel();
		
		JPanel pAnimalOperations = new JPanel();
		GroupLayout lAnimalOperations = new GroupLayout(pAnimalOperations);
		pAnimalOperations.setLayout(lAnimalOperations);
		
		JPanel emptyPanel2 = new JPanel(new BorderLayout());
		
		lAnimalOperations.setHorizontalGroup(lAnimalOperations.createParallelGroup()
				.addGroup(lAnimalOperations.createSequentialGroup()
						.addComponent(pAdd)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 1, Short.MAX_VALUE)
						.addComponent(pEdit)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 1, Short.MAX_VALUE)
						.addComponent(pDelete))
				.addGroup(lAnimalOperations.createSequentialGroup()
					.addComponent(lStatus)
					.addComponent(emptyPanel2)
					.addGroup(lAnimalOperations.createParallelGroup()
							.addComponent(pSaveChanges)
							.addComponent(pUndoChanges))));

		lAnimalOperations.setVerticalGroup(lAnimalOperations.createSequentialGroup()
				.addGroup(lAnimalOperations.createParallelGroup()
						.addComponent(pAdd)
						.addComponent(pEdit)
						.addComponent(pDelete))
				.addGroup(lAnimalOperations.createParallelGroup()
						.addComponent(lStatus)
						.addComponent(emptyPanel2)
						.addGroup(lAnimalOperations.createSequentialGroup()
							.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 1, Short.MAX_VALUE)	
							.addComponent(pSaveChanges)
							.addComponent(pUndoChanges))));


		pAnimalOperations.setBorder(BorderFactory.createTitledBorder("Animal"));
				
		GroupLayout layout = new GroupLayout(panel);
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		panel.setLayout(layout);
		
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)				
						.addComponent(pAnimalOperations)
						.addComponent(bLogout));

		layout.setVerticalGroup(layout.createSequentialGroup()
				.addComponent(pAnimalOperations)
				
				.addComponent(bLogout)
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE));	
			
		panel.setBorder(BorderFactory.createTitledBorder("Control"));
		
		return panel;
	}
	
	/**
	 * Create panel containing details of selected animal; 
	 * to edit animal info
	 * @return	detail panel
	 */
	private JComponent createAnimalDetailPanel() {
		
		lDGenus = new JLabel("Genus");
		lDSpecies = new JLabel("Species");
		lDOrder = new JLabel("Order");
		lDPopulation = new JLabel("Population");
		lDPopulationFrom = new JLabel("Population from");
		lDPopulationTo = new JLabel("Population to");		
		lDDescribtion = new JLabel("Describtion");	
		lDLawProtection = new JLabel("Law protected");	
		
		tDGenus = new JTextField();
		tDSpecies = new JTextField();
		cDLawProtection = new JCheckBox();
		tDPopulation = new JTextField();
		
		calDPopulationFrom = new Calendar(Calendar.HORIZONTAL);
		calDPopulationTo = new Calendar(Calendar.HORIZONTAL);
		
		tDOrder = new JComboBox(orderStrings);
		tDOrder.setSelectedIndex(0);
		tDOrder.setEditable(false);
		tDOrder.setEnabled(false);
		
		bDDeletePopulation = new JButton("Delete");
		bDDeletePopulation.addActionListener(this);
		bDAddPopulation = new JButton("Add");
		bDAddPopulation.addActionListener(this);
		bDRotateImage = new JButton("Rotate 90�");
		bDRotateImage.addActionListener(this);
		
		JPanel pDDeleteButton = new JPanel();
		pDDeleteButton.setLayout(new BorderLayout());
		pDDeleteButton.add(bDDeletePopulation);
		
		JPanel pDAddButton = new JPanel();
		pDAddButton.setLayout(new BorderLayout());
		pDAddButton.add(bDAddPopulation);
		
		JPanel pDRotateImage = new JPanel();
		pDRotateImage.setLayout(new BorderLayout());
		pDRotateImage.add(bDRotateImage);		
		
		listDPopulationTimeline = new JList<Object>();
		
		if(selectedAnimalIndexInList != -1)
			listPopulationTimeline(selectedAnimal);
		
		listDPopulationTimeline.setCellRenderer(new PopulationTimelineCellRenderer());
		listDPopulationTimeline.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listDPopulationTimeline.setLayoutOrientation(JList.VERTICAL);
		
		tDDescribtion = new JTextArea(TA_ROWS, TA_COLS);
		
		tDImagePreview = new JPanel();
		lDImagePreview = new JLabel();
				
		tDGenus.setPreferredSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
		tDSpecies.setPreferredSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
		tDOrder.setPreferredSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
		tDPopulation.setPreferredSize(new Dimension(SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT));
				
		tDImagePreview.setMaximumSize(new Dimension(DETAIL_IMAGE_WIDTH, DETAIL_IMAGE_HEIGHT));
		tDImagePreview.setMinimumSize(new Dimension(DETAIL_IMAGE_WIDTH, DETAIL_IMAGE_WIDTH));
		tDImagePreview.setBackground(Color.black);
		tDImagePreview.setLayout(new BorderLayout());
		tDImagePreview.add(lDImagePreview);
		
		
		tDDescribtion.setWrapStyleWord(true);
		tDDescribtion.setLineWrap(true);
		
		enableDetailPanel(false);

		JScrollPane scrollDescribtion = new JScrollPane(tDDescribtion); 
		
		JScrollPane scrollPopulationTimeline = new JScrollPane(listDPopulationTimeline);
		scrollPopulationTimeline.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		JPanel panel = new JPanel();		
		GroupLayout layout = new GroupLayout(panel);
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		panel.setLayout(layout);
		
		layout.setHorizontalGroup(
				layout.createSequentialGroup()
					.addComponent(tDImagePreview)
					.addGroup(
							layout.createParallelGroup()
									.addComponent(lDGenus)
									.addComponent(lDSpecies)
									.addComponent(lDOrder)
									.addComponent(lDLawProtection)
									.addComponent(pDRotateImage, Alignment.LEADING, DETAIL_ROTATE_BUTTON_WIDTH, DETAIL_ROTATE_BUTTON_WIDTH, DETAIL_ROTATE_BUTTON_WIDTH))
					.addGroup(
							layout.createParallelGroup()
									.addComponent(tDGenus, Alignment.LEADING, DETAIL_TEXTFIELD_WIDTH, DETAIL_TEXTFIELD_WIDTH, DETAIL_TEXTFIELD_WIDTH)
									.addComponent(tDSpecies, Alignment.LEADING, DETAIL_TEXTFIELD_WIDTH, DETAIL_TEXTFIELD_WIDTH, DETAIL_TEXTFIELD_WIDTH)
									.addComponent(tDOrder, Alignment.LEADING, DETAIL_TEXTFIELD_WIDTH, DETAIL_TEXTFIELD_WIDTH, DETAIL_TEXTFIELD_WIDTH)
									.addComponent(cDLawProtection))
					.addGroup(
							layout.createParallelGroup()
									.addComponent(lDPopulation)
									.addComponent(lDPopulationFrom)
									.addComponent(lDPopulationTo)
									.addComponent(pDAddButton, Alignment.LEADING, DETAIL_POPULATION_BUTTON_WIDTH, DETAIL_POPULATION_BUTTON_WIDTH, DETAIL_POPULATION_BUTTON_WIDTH)
									.addComponent(pDDeleteButton, Alignment.LEADING, DETAIL_POPULATION_BUTTON_WIDTH, DETAIL_POPULATION_BUTTON_WIDTH, DETAIL_POPULATION_BUTTON_WIDTH))
					.addGroup(
							layout.createParallelGroup()
									.addComponent(tDPopulation, Alignment.LEADING, DETAIL_TEXTFIELD_WIDTH, DETAIL_TEXTFIELD_WIDTH, DETAIL_TEXTFIELD_WIDTH)
									.addComponent(calDPopulationFrom, Alignment.LEADING, DETAIL_CALENDAR_WIDTH, DETAIL_CALENDAR_WIDTH, DETAIL_CALENDAR_WIDTH)
									.addComponent(calDPopulationTo, Alignment.LEADING, DETAIL_CALENDAR_WIDTH, DETAIL_CALENDAR_WIDTH, DETAIL_CALENDAR_WIDTH)
									.addComponent(scrollPopulationTimeline, Alignment.LEADING, DETAIL_POPULATION_TIMELINE_WIDTH, DETAIL_POPULATION_TIMELINE_WIDTH, DETAIL_POPULATION_TIMELINE_WIDTH))
					.addGroup(
							layout.createParallelGroup()
									.addComponent(lDDescribtion)
									.addComponent(scrollDescribtion)));
		
		layout.setVerticalGroup(
				layout.createParallelGroup()
					.addComponent(tDImagePreview)
					.addGroup(
							layout.createSequentialGroup()
									.addGroup(
											layout.createParallelGroup()
													.addComponent(lDGenus)
													.addComponent(tDGenus, Alignment.LEADING, DETAIL_TEXTFIELD_HEIGHT, DETAIL_TEXTFIELD_HEIGHT, DETAIL_TEXTFIELD_HEIGHT)
													.addComponent(lDPopulation)
													.addComponent(tDPopulation, Alignment.LEADING, DETAIL_TEXTFIELD_HEIGHT, DETAIL_TEXTFIELD_HEIGHT, DETAIL_TEXTFIELD_HEIGHT))

									.addGroup(
											layout.createParallelGroup()
													.addComponent(lDSpecies)
													.addComponent(tDSpecies, Alignment.LEADING, DETAIL_TEXTFIELD_HEIGHT, DETAIL_TEXTFIELD_HEIGHT, DETAIL_TEXTFIELD_HEIGHT)
													.addComponent(lDPopulationFrom)
													.addComponent(calDPopulationFrom, Alignment.LEADING, DETAIL_CALENDAR_HEIGHT, DETAIL_CALENDAR_HEIGHT, DETAIL_CALENDAR_HEIGHT))

									.addGroup(
											layout.createParallelGroup()
													.addComponent(lDOrder)
													.addComponent(tDOrder, Alignment.LEADING, DETAIL_TEXTFIELD_HEIGHT, DETAIL_TEXTFIELD_HEIGHT, DETAIL_TEXTFIELD_HEIGHT)
													.addComponent(lDPopulationTo)
													.addComponent(calDPopulationTo, Alignment.LEADING, DETAIL_CALENDAR_HEIGHT, DETAIL_CALENDAR_HEIGHT, DETAIL_CALENDAR_HEIGHT))
									.addGroup(
											layout.createParallelGroup()
													.addGroup(layout.createSequentialGroup()
																.addComponent(lDLawProtection)
																.addComponent(pDRotateImage))
													.addComponent(cDLawProtection, Alignment.LEADING, DETAIL_TEXTFIELD_HEIGHT, DETAIL_TEXTFIELD_HEIGHT, DETAIL_TEXTFIELD_HEIGHT)
													.addGroup(layout.createSequentialGroup()
															.addComponent(pDAddButton)
															.addComponent(pDDeleteButton))
													.addComponent(scrollPopulationTimeline, Alignment.LEADING, DETAIL_POPULATION_TIMELINE_HEIGHT, DETAIL_POPULATION_TIMELINE_HEIGHT, DETAIL_POPULATION_TIMELINE_HEIGHT)))
					.addGroup(
							layout.createSequentialGroup()
								.addComponent(lDDescribtion)
								.addComponent(scrollDescribtion)));
		
		panel.setBorder(BorderFactory.createTitledBorder("Detail"));
		
		return panel;
	}
	
	/**
	 * Enable animal detail panel. Enabled only to edit or add new animal.
	 * @param enabled 
	 */
	private void enableDetailPanel(boolean enabled) {
		tDGenus.setEditable(enabled);
		tDSpecies.setEditable(enabled);
		tDOrder.setEnabled(enabled);
		cDLawProtection.setEnabled(enabled);
		tDPopulation.setEditable(enabled);		
		tDDescribtion.setEditable(enabled);		
		tDImagePreview.setEnabled(enabled);
		bDDeletePopulation.setEnabled(enabled);
		bDAddPopulation.setEnabled(enabled);
		calDPopulationFrom.setEnabled(enabled);
		calDPopulationTo.setEnabled(enabled);
		bDRotateImage.setEnabled(enabled);
		
		if(enabled == true)
			tDImagePreview.addMouseListener(this);
		else
			tDImagePreview.removeMouseListener(this);
		
		shapeTypesComboBox.setEnabled(enabled);
	}
	
	/**
	 * Clear all fields of animal detail panel
	 */
	private void clearDetailPanel() {
		tDGenus.setText("");
		tDSpecies.setText("");
		tDOrder.setSelectedIndex(0);
		cDLawProtection.setSelected(false);
		tDPopulation.setText("");	
		tDDescribtion.setText("");
		lAreaSize.setText(" ");
		
		aDNewAnimal = new Animal(-1, "", "", "", new ArrayList<Population>(), false, "");
		
		listPopulationTimeline(aDNewAnimal);
		listModelTimeline.clear();
		
		lDImagePreview.setIcon(null);
		newAnimalIcon = null;
		
		lMapContainer.clear();
	}
	
	/**
	 * Display animal detail info
	 */
	private void displayAnimalDetail() {		
		tDGenus.setText(selectedAnimal.getGenusName());
		tDSpecies.setText(selectedAnimal.getSpeciesName());
		tDOrder.setSelectedItem(selectedAnimal.getOrder());
		cDLawProtection.setSelected(selectedAnimal.getLawProtection());
		
		try {
			lAreaSize.setText("Area size before edit: " + db.getAnimalAreaSize(selectedAnimal.getId())
								 + "km^2");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(selectedAnimalIndexInList != -1)
			listPopulationTimeline(selectedAnimal);
		
		tDDescribtion.setText(selectedAnimal.getDescription());
		
		refreshDetailImage();
	}
	
	/**
	 * Refresh picture of selected animal
	 */
	private void refreshDetailImage() {
		Dimension scaledIconDimension;
		Image newImg;
		ImageIcon rawIcon, scaledIcon = null;
		
		OrdImage image;
		
		try {
			image = db.savePhotoToFile(selectedAnimal.getId());
			
			if(image == null)	return;
			
			rawIcon = new ImageIcon(image.getDataInByteArray());
			
			scaledIconDimension = getScaledDimension(	new Dimension(rawIcon.getIconWidth(), rawIcon.getIconHeight()),
														new Dimension(tDImagePreview.getWidth(), tDImagePreview.getHeight())
														);
			
			newImg = rawIcon.getImage();
			newImg = newImg.getScaledInstance(	scaledIconDimension.width,
													scaledIconDimension.height, 
													java.awt.Image.SCALE_SMOOTH 
													);  
			scaledIcon = new ImageIcon( newImg );
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		
		lDImagePreview.setIcon(scaledIcon);	
	}
	
	/**
	 * Catch checkbox' events
	 */
	@Override
	public void itemStateChanged(ItemEvent event) {
		Object source = event.getSource();
		
		if(source == cGenus) {
			if(cGenus.isSelected()) {
				tGenus.setEnabled(true);
				if(cImage.isSelected())	cImage.setSelected(false);
				if(cAreaIntersection.isSelected())	cAreaIntersection.setSelected(false);
				if(cBiggestArea.isSelected())	cBiggestArea.setSelected(false);
			}
			else {
				tGenus.setEnabled(false);
			}
		}
		else if(source == cSpecies) {
			if(cSpecies.isSelected()) {
				tSpecies.setEnabled(true);
				if(cImage.isSelected())	cImage.setSelected(false);
				if(cAreaIntersection.isSelected())	cAreaIntersection.setSelected(false);
				if(cBiggestArea.isSelected())	cBiggestArea.setSelected(false);
			}
			else {
				tSpecies.setEnabled(false);
			}
		}
		else if(source == cOrder) {
			if(cOrder.isSelected()) {
				tOrder.setEnabled(true);
				if(cImage.isSelected())	cImage.setSelected(false);
				if(cAreaIntersection.isSelected())	cAreaIntersection.setSelected(false);
				if(cBiggestArea.isSelected())	cBiggestArea.setSelected(false);
			}
			else {
				tOrder.setEnabled(false);
			}
		}
		else if(source == cLawProtection) {
			if(cLawProtection.isSelected()) {
				tLawProtection.setEnabled(true);
				if(cImage.isSelected())	cImage.setSelected(false);
				if(cAreaIntersection.isSelected())	cAreaIntersection.setSelected(false);
				if(cBiggestArea.isSelected())	cBiggestArea.setSelected(false);
			}
			else {
				tLawProtection.setEnabled(false);
			}
		}
		else if(source == cPopulationBelow) {
			if(cPopulationBelow.isSelected()) {
				tPopulationBelow.setEnabled(true);
				if(cImage.isSelected())	cImage.setSelected(false);
				if(cAreaIntersection.isSelected())	cAreaIntersection.setSelected(false);
				if(cBiggestArea.isSelected())	cBiggestArea.setSelected(false);
			}
			else {
				tPopulationBelow.setEnabled(false);
			}
		}
		else if(source == cPopulationAbove) {
			if(cPopulationAbove.isSelected()) {
				tPopulationAbove.setEnabled(true);
				if(cImage.isSelected())	cImage.setSelected(false);
				if(cAreaIntersection.isSelected())	cAreaIntersection.setSelected(false);
				if(cBiggestArea.isSelected())	cBiggestArea.setSelected(false);
			}
			else {
				tPopulationAbove.setEnabled(false);
			}
		}
		else if(source == cPopulationAt) {
			if(cPopulationAt.isSelected()) {
				calPopulationAt.setEnabled(true);
				if(cImage.isSelected())	cImage.setSelected(false);
				if(cAreaIntersection.isSelected())	cAreaIntersection.setSelected(false);
				if(cBiggestArea.isSelected())	cBiggestArea.setSelected(false);
			}
			else {
				calPopulationAt.setEnabled(false);
				lAnimalCount.setText(" ");
			}
		}
		else if(source == cImage) {
			if(cImage.isSelected()) {
				cGenus.setSelected(false);
				cSpecies.setSelected(false);
				cOrder.setSelected(false);
				cPopulationAbove.setSelected(false);
				cPopulationBelow.setSelected(false);
				cPopulationAt.setSelected(false);
				cLawProtection.setSelected(false);
				if(cAreaIntersection.isSelected())	cAreaIntersection.setSelected(false);
				if(cBiggestArea.isSelected())	cBiggestArea.setSelected(false);
				
				bImage.setEnabled(true);
			}
			else {
				bImage.setEnabled(false);
			}
		}
		else if(source == cAreaIntersection) {
			if(cAreaIntersection.isSelected()) {
				cGenus.setSelected(false);
				cSpecies.setSelected(false);
				cOrder.setSelected(false);
				cPopulationAbove.setSelected(false);
				cPopulationBelow.setSelected(false);
				cPopulationAt.setSelected(false);
				cLawProtection.setSelected(false);
				cBiggestArea.setSelected(false);

				if(cImage.isSelected())	cImage.setSelected(false);
				if(cBiggestArea.isSelected())	cBiggestArea.setSelected(false);
			}
			else {
				cAreaIntersection.setSelected(false);
			}
		}
		else if(source == cBiggestArea) {
			if(cBiggestArea.isSelected()) {
				cGenus.setSelected(false);
				cSpecies.setSelected(false);
				cOrder.setSelected(false);
				cPopulationAbove.setSelected(false);
				cPopulationBelow.setSelected(false);
				cPopulationAt.setSelected(false);
				cLawProtection.setSelected(false);
				cAreaIntersection.setSelected(false);

				if(cImage.isSelected())	cImage.setSelected(false);
				if(cAreaIntersection.isSelected())	cAreaIntersection.setSelected(false);
			}
			else {
				cBiggestArea.setSelected(false);
			}
		}
	}
	
	/**
	 * Catch choose image, search buttons, control buttons events...
	 * @param event
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource();
		
		if(source == bImage) {			
			Dimension scaledIconDimension;
			Image image = null;
			Image newImg;
			int returnVal;
			JFileChooser fileChooser;
			ImageIcon scaledIcon;
			
			fileChooser = new JFileChooser();
			fileChooser.setDialogTitle("Choose photo");
			
			returnVal = fileChooser.showDialog(this, "Choose..");
						         
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				searchFile = fileChooser.getSelectedFile();
			}
						
			if(searchFile == null)	return;
			
			try	{
			    image = ImageIO.read(searchFile);      
			} catch (IOException e) {
				e.printStackTrace();
			}			
			
			scaledIconDimension = getScaledDimension(	new Dimension(image.getWidth(null), image.getHeight(null)),
																new Dimension(pImagePreview.getWidth(), pImagePreview.getHeight())
																);
			
			newImg = image.getScaledInstance(	scaledIconDimension.width,
													scaledIconDimension.height, 
													java.awt.Image.SCALE_SMOOTH 
													);  
			scaledIcon = new ImageIcon( newImg );
			
			lImagePreview.setIcon(scaledIcon);			
		}
		else if(source == bSearch) {	
			
			Cursor cursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
			setCursor(cursor);
			
			lStatus.setText("Searching...");

			javax.swing.SwingUtilities.invokeLater(new Runnable() {
		        public void run(){
		    
		        	if(cPopulationAt.isSelected()) {
		        		try {
							lAnimalCount.setText("Total population at selected date: " + db.sumAnimalsAtDate(calPopulationAt.getDate()));
						} catch (SQLException e) {
							e.printStackTrace();
						}
		        	}
		        	
					list.clear();
					list = searchAnimal();
					listSearchResult(list);
						
					Cursor cursor = Cursor.getDefaultCursor();
					setCursor(cursor);
						
					lStatus.setText("Done");
		        }
		    });
			
		}
		else if(source == bAdd) {
			clearDetailPanel();
			enableDetailPanel(true);
			
			bDRotateImage.setEnabled(false);
			//bDDeletePopulation.setEnabled(false);
			//bDAddPopulation.setEnabled(false);
			
			lMapContainer.setColor(new Color(0.0f, 0.5f, 1.0f, 0.5f));
			
			state = States.ADD;
		}
		else if(source == bEdit) {
			if(selectedAnimalIndexInList != -1) {
				displayAnimalDetail();
				enableDetailPanel(true);
			
				state = States.EDIT;
			}
		}
		else if(source == bDelete) {
			if(selectedAnimalIndexInList == -1)	return;
			try {
				db.deleteAnimal(selectedAnimal.getId());
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			List<Animal> list = searchAnimal();
			listSearchResult(list);
		}
		else if(source == bSaveChanges) {
			switch(state){
			case IDLE:
				break;
			case ADD:
				// Add to database		
				aDNewAnimal.setOrder((String)(tDOrder.getSelectedItem()));
				aDNewAnimal.setDescription(tDDescribtion.getText());
				aDNewAnimal.setGenusName(tDGenus.getText());
				aDNewAnimal.setSpeciesName(tDSpecies.getText());
				aDNewAnimal.setLawProtection(cDLawProtection.isSelected());
				
				try {
					int id = db.insertAnimal(aDNewAnimal.getGenusName(), aDNewAnimal.getSpeciesName(), aDNewAnimal.getOrder(), 
							aDNewAnimal.getLawProtection(), aDNewAnimal.getDescription());
					
					if(newAnimalIcon != null) {						
						db.loadPhotoFromFile(id, newAnimalIcon.getAbsolutePath());
					}
					
					List<ShapeCoords> shape = lMapContainer.normalizeShape(); 
					
					if(shape != null && shape.size() > 0) {
						//////////////////////////////////////////////////////////////
						//					INSERT SHAPE							//
						db.insertAnimalHabitat(id, shape);
						lMapContainer.clear();
						//////////////////////////////////////////////////////////////
					}
					
					
					for(int i = 0; i < aDNewAnimal.getPopulation().size(); i++) {
						Population p = aDNewAnimal.getPopulation().get(i);
						SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
						String dateFrom = "" + format.format(p.getValidFrom());
						String dateTo = "" + format.format(p.getValidTo());
						
						try {							
							db.addPop(id, dateFrom, dateTo, p.getPopulation());
						} catch(SQLException e) { e.printStackTrace(); }
					}
				} catch (SQLException | IOException e) {
					e.printStackTrace();
				}
				
				clearDetailPanel();
				enableDetailPanel(false);
				
				listSearchResult(db.loadAnimalsFromDB());
								
				state = States.IDLE;

				
				break;
			case EDIT:

				try {
					db.updateAnimal(selectedAnimal.getId(), tDGenus.getText(), tDSpecies.getText(), (String)tDOrder.getSelectedItem(), 
						 cDLawProtection.isSelected(), tDDescribtion.getText());
					
					List<ShapeCoords> shape = lMapContainer.normalizeShape(); 
					
					if(shape != null && shape.size() > 0) {
						//////////////////////////////////////////////////////////////
						//				UPDATE alebo INSERT SHAPE					//
						if(lMapContainer.getStartShapeCount() > 0) {
//							UPDATE
							db.deleteHabitat(selectedAnimal.getId());
							db.insertAnimalHabitat(selectedAnimal.getId(), shape);
							lMapContainer.clear();

						}
						else {
//							NEW
							db.insertAnimalHabitat(selectedAnimal.getId(), shape);
							lMapContainer.clear();

						}
						//////////////////////////////////////////////////////////////
					}
					else {
						//////////////////////////////////////////////////////////////
						//				DELETE SHAPE								//
						if(lMapContainer.getStartShapeCount() > 0) {
							db.deleteHabitat(selectedAnimal.getId());
						}
						//////////////////////////////////////////////////////////////
					}
					
					if(newAnimalIcon != null) {
						db.deletePhoto(selectedAnimal.getId());
						
						db.loadPhotoFromFile(selectedAnimal.getId(), newAnimalIcon.getAbsolutePath());
					}
					
				} catch(SQLException | IOException e) {
					e.printStackTrace();
				}
				
				clearDetailPanel();
				enableDetailPanel(false);
				
				
				List<Animal> list = searchAnimal();
				listSearchResult(list);
				
				state = States.IDLE;
				
				selectedAnimalIndexInList = -1;
			
				animalsList.clearSelection();
				
				break;
			default:
				break;
			}	
		}
		else if(source == bDAddPopulation) {
			if(state.equals(States.EDIT)) {
				if(tDPopulation.getText().isEmpty())	return;
				
				DateFormat format = new SimpleDateFormat("dd-MMM-yy");
				java.util.Date parsedFrom;
				java.util.Date parsedTo;
				
				try {
					parsedFrom = format.parse(calDPopulationFrom.getDate());
					parsedTo = format.parse(calDPopulationTo.getDate());

					if(parsedFrom.after(parsedTo)) {
						return;
					}
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
		
				
				List<Population> population = new ArrayList<Population>();
	
				try {
					db.addPop(selectedAnimal.getId(), calDPopulationFrom.getDate(), calDPopulationTo.getDate(), Integer.parseInt(tDPopulation.getText()));
					population = db.getAnimalPopulation(selectedAnimal.getId());
				} catch(SQLException e) { e.printStackTrace(); }
				
				list.get(selectedAnimalIndexInList).setPopulation(population);
				
				int tmpIndex = selectedAnimalIndexInList;
				
				populationModifyFlag = true;
				
				listSearchResult(list);
				populationModifyFlag = true;

				selectedAnimalIndexInList = tmpIndex;
				
				animalsList.setSelectedIndex(selectedAnimalIndexInList);
				enableDetailPanel(true);
				
				state = States.EDIT;
			}
			else if(state.equals(States.ADD)) {
				if(tDPopulation.getText().isEmpty())	return;
				
				DateFormat format = new SimpleDateFormat("dd-MMM-yy");
				java.util.Date parsedFrom;
				java.util.Date parsedTo;
				
				List<Population> population = aDNewAnimal.getPopulation();
	
				try {
					parsedFrom = format.parse(calDPopulationFrom.getDate());
					parsedTo = format.parse(calDPopulationTo.getDate());
			
					if(parsedFrom.after(parsedTo)) {
						listPopulationTimeline(aDNewAnimal);
						tDPopulation.setText("");
						return;
					}
					
					population.add(new Population(Integer.parseInt(tDPopulation.getText()), new Date(parsedFrom.getTime()), new Date(parsedTo.getTime())));
					
					aDNewAnimal.setPopulation(population);

				} catch (ParseException e) {
					e.printStackTrace();
				}	
				
				listPopulationTimeline(aDNewAnimal);
				tDPopulation.setText("");
			}
		}
		else if(source == bDDeletePopulation) {
			if(state.equals(States.EDIT)) {
				if(selectedAnimalIndexInList == -1) return;
				if(listDPopulationTimeline.getSelectedIndex() == -1) return;
				
				List<Population> population = new ArrayList<Population>();
				SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy");
				
				String date = "" + format.format(selectedAnimal.getPopulation().get(listDPopulationTimeline.getSelectedIndex()).getValidFrom());
							
	
				try {
					
					
					db.deletePop(selectedAnimal.getId(), date);
					
					population = db.getAnimalPopulation(selectedAnimal.getId());
					
					
				} catch(SQLException e) { e.printStackTrace(); }
				
				list.get(selectedAnimalIndexInList).setPopulation(population);
				
				int tmpIndex = selectedAnimalIndexInList;
				
				populationModifyFlag = true;
				
				listSearchResult(list);
				populationModifyFlag = true;

				selectedAnimalIndexInList = tmpIndex;
				
				animalsList.setSelectedIndex(selectedAnimalIndexInList);
				enableDetailPanel(true);
				
				state = States.EDIT;
			}
			else if(state.equals(States.ADD)) {
				if(aDNewAnimal.getPopulation() == null || listDPopulationTimeline.getSelectedIndex() < 0) {
					return;
				}

				List<Population> population = aDNewAnimal.getPopulation();
				population.remove(listDPopulationTimeline.getSelectedIndex());
				
				aDNewAnimal.setPopulation(population);
				
				listPopulationTimeline(aDNewAnimal);
			}
		}
		else if(source == bDRotateImage) {
			if(selectedAnimalIndexInList == -1) return;
			
			try {
				db.pictureRotation(selectedAnimal.getId(), ROTATION_ANGLE);
			} catch(SQLException e) { e.printStackTrace(); }
			
			
			int tmpIndex = selectedAnimalIndexInList;
			rotationModifyFlag = true;

			listSearchResult(list);
			rotationModifyFlag = true;

			selectedAnimalIndexInList = tmpIndex;
			
			animalsList.setSelectedIndex(selectedAnimalIndexInList);
			enableDetailPanel(true);
			
			state = States.EDIT;
		}
		else if(source == shapeTypesComboBox) {			
			lMapContainer.changeShapeType(shapeTypesComboBox.getSelectedIndex());
		}
		else if(source == bGetData) {
			
			/**
			 * NACITAVANIE DATA PRI STARTE
			 */
			list = new ArrayList<Animal>();
			
			Cursor cursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
			setCursor(cursor);
			
			
			lStatus.setText("Creating db. Retrieving data...");

			javax.swing.SwingUtilities.invokeLater(new Runnable() {
		        public void run(){
		    
					try {
						
						list = db.loadAnimalsFromDB();
						
						if(list == null) {
							db.createTables();
							try {
								db.addAnimals();
							} catch (IOException e) {
								e.printStackTrace();
							}
							
							list = db.loadAnimalsFromDB();
						}
						
						Cursor cursor = Cursor.getDefaultCursor();
						setCursor(cursor);
						
						listSearchResult(list);
						
						lStatus.setText("Done");

					} catch(SQLException e) {
						e.printStackTrace();
					} 					
		        }
		    });
			
			
		}
		else if(source == bLogout) {
			lMapContainer.clear();
			Main.Logout();
		}
	}
	
	/**
	 * Searches all animals in database that meet the search criteria
	 * @return list of found animals
	 */
	private List<Animal> searchAnimal() {	
		List<Animal> resultSet = new ArrayList<Animal>();
		
		animalSearchInfo.clear();
		
		if(cGenus.isSelected()) {
			animalSearchInfo.put("genusName = ", tGenus.getText());
		}
		if(cSpecies.isSelected()) {
			animalSearchInfo.put("speciesName = ", tSpecies.getText());
		}
		if(cOrder.isSelected()) {
			animalSearchInfo.put("orderName = ", (String)tOrder.getSelectedItem());
		}
		if(cLawProtection.isSelected()) {
			animalSearchInfo.put("lawProtection = ", Integer.toString((tLawProtection.isSelected())? 1 : 0));
		}
		if(cPopulationBelow.isSelected()) {
			animalSearchInfo.put("population.population < ", tPopulationBelow.getText());
		}
		if(cPopulationAbove.isSelected()) {
			animalSearchInfo.put("population.population > ", tPopulationAbove.getText());
		}
		if(cPopulationAt.isSelected()) {
			animalSearchInfo.put("population.validfrom <= ", calPopulationAt.getDate());
			animalSearchInfo.put("population.validto > ", calPopulationAt.getDate());
		}
		if(cImage.isSelected()) {
			if(searchFile == null)	return resultSet;
			String path;
			
			path = searchFile.getAbsolutePath();
			
			try {
				resultSet = db.searchByPhoto(path);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			return resultSet;
		}
		
		if(cAreaIntersection.isSelected()) {
			// hladaj intersection
			if(selectedAnimalIndexInList > -1) {
				try {
					resultSet = db.getAnimalsSharingSameHabitat(selectedAnimal.getId());
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				return resultSet;
			}
			else {
				return resultSet;
			}
		}
		if(cBiggestArea.isSelected()) {
			// hladaj podla velkosti uzemia
			try {
				resultSet = db.getAnimalsByArea();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			return resultSet;
		}
		
		
		if(animalSearchInfo.isEmpty()) {
			resultSet = db.loadAnimalsFromDB();
		}
		else {
			try {
				resultSet = db.searchAnimals(animalSearchInfo);
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}
		
		return resultSet;
	}
	
	/**
	 * List search result to the Jlist in search panel 
	 * @param resultSet
	 */
	private void listSearchResult(List<Animal> resultSet) {
		
		listModel = new DefaultListModel<Object>();
		
		for(Animal a : resultSet){
	         listModel.addElement(a);
	    }
		
		animalsList.setModel(listModel);
	}
	
	/**
	 * List populations of selected animal to the JList in detail panel
	 * @param animal
	 */
	private void listPopulationTimeline(Animal animal) {
		
		listModelTimeline = new DefaultListModel<Object>();
		
		for(Population i : animal.getPopulation()) {
			listModelTimeline.addElement((Population)i);
		}
		
		
		
		listDPopulationTimeline.setModel(listModelTimeline);
	}
	
	/**
	 * Calculates scaled dimension keeping aspect ratio
	 * @param image		-	source image dimension
	 * @param bounds	-	bounds to fit image in
	 * @return			-	scaled dimension
	 */
	public Dimension getScaledDimension(Dimension image, Dimension bounds) {
		Dimension result = new Dimension();
		
		if(image.getWidth() > bounds.getWidth())
		{
			result.width = (int) bounds.getWidth();
			result.height = (int) ((result.width * image.getHeight()) / image.getWidth());
		}
		if(result.getHeight() > bounds.getHeight() || result.getHeight() == 0)
		{
			result.height = (int) bounds.getHeight();
			result.width = (int) ((result.height * image.getWidth()) / image.getHeight());
		}
		return result;
	}
	
	/**
	 * Custom cell renderer for JList of search result
	 */
	class CellRenderer extends JLabel implements ListCellRenderer {

		private final Color HIGHLIGHT_COLOR = new Color(0, 100, 180);
		
		public CellRenderer() {
			setOpaque(true);
		    setIconTextGap(12);
		}
		
		@Override
		public Component getListCellRendererComponent(JList list, Object animal, int index, boolean isSelected, boolean hasFocus) {
			Animal a = (Animal) animal;
			
			setText(a.getGenusName() + " " + a.getSpeciesName());
			
			if(isSelected){
				setBackground(HIGHLIGHT_COLOR);
				setForeground(Color.white);
			}
			else {
				setBackground(Color.white);
				setForeground(Color.black);
			}
			
			return this;
		}
		
	}
	
	/**
	 * Custom cell renderer for JList of population
	 *
	 */
	class PopulationTimelineCellRenderer extends JLabel implements ListCellRenderer {

		private static final long serialVersionUID = 1L;

		private final Color HIGHLIGHT_COLOR = new Color(0, 100, 180);
		
		public PopulationTimelineCellRenderer() {
			setOpaque(true);
		    setIconTextGap(12);
		}
		
		@Override
		public Component getListCellRendererComponent(JList list, Object population, int index, boolean isSelected, boolean hasFocus) {
			Population p = (Population) population;
			
			String data = "";
			
			data += p.getValidFrom() + " " + p.getPopulation() + " " + p.getValidTo(); 
			
			setText(data);
			
			if(isSelected){
				setBackground(HIGHLIGHT_COLOR);
				setForeground(Color.white);
			}
			else {
				setBackground(Color.white);
				setForeground(Color.black);
			}
			
			return this;
		}
		
	}
	
	/**
	 * Date picker class
	 * @author Roman
	 *
	 */
	class Calendar extends JPanel {
		
		
		private static final long serialVersionUID = 1L;
		
		private String[] daysStrings = new String[31];
		private String[] monthsStrings = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
		private String[] yearsStrings = new String[31];
		
		private JComboBox<String> cDays;
		private JComboBox<String> cMonths;
		private JComboBox<String> cYears;
		
		private static final int VERTICAL = 1;
		private static final int HORIZONTAL = 0;
		
		public Calendar(int orientation) {
			
			for(int i = 0; i < 31; i++) {	
				daysStrings[i] = Integer.toString(i + 1);
				yearsStrings[i] = Integer.toString(2015 - i);
			}
			
			cDays = new JComboBox<String>(daysStrings);
			cMonths = new JComboBox<String>(monthsStrings);
			cYears = new JComboBox<String>(yearsStrings);
			
			JLabel lDay = new JLabel("Day");
			JLabel lMonth = new JLabel("Month");
			JLabel lYear = new JLabel("Year");
			
			GroupLayout layout = new GroupLayout(this);
			layout.setAutoCreateGaps(true);
			setLayout(layout);
			
			if(orientation == VERTICAL) {
				layout.setHorizontalGroup(
						layout.createSequentialGroup()
							.addGroup(
									layout.createParallelGroup()
										.addComponent(lDay)
										.addComponent(lMonth)
										.addComponent(lYear))
							.addGroup(
									layout.createParallelGroup()
									
										.addComponent(cDays)
										.addComponent(cMonths)
										.addComponent(cYears))
						);
				
				layout.setVerticalGroup(
						layout.createSequentialGroup()
							.addGroup(
									layout.createParallelGroup()
										.addComponent(lDay)
										.addComponent(cDays))
							.addGroup(
									layout.createParallelGroup()
										.addComponent(lMonth)
										.addComponent(cMonths))
							.addGroup(
									layout.createParallelGroup()
										.addComponent(lYear)
										.addComponent(cYears))
						);
			}
			else {	
				layout.setHorizontalGroup(
						layout.createSequentialGroup()
							.addComponent(lDay)
							.addComponent(cDays)
							.addComponent(lMonth)
							.addComponent(cMonths)
							.addComponent(lYear)
							.addComponent(cYears)
						);
				
				layout.setVerticalGroup(
						layout.createParallelGroup()
							.addComponent(lDay)
							.addComponent(cDays)
							.addComponent(lMonth)
							.addComponent(cMonths)
							.addComponent(lYear)
							.addComponent(cYears)
						);	
			}			
		}
		
		public String getDate() {
			return (String)cDays.getSelectedItem() + "-" + (String)cMonths.getSelectedItem() + "-" + ((String)cYears.getSelectedItem()).substring(2, 4); 
		}
		
		@Override
		public void setEnabled(boolean enabled) {
			cDays.setEnabled(enabled);
			cMonths.setEnabled(enabled);
			cYears.setEnabled(enabled);
		}
	}

	/**
	 * Custom label to draw polygons on map of slovakia
	 *
	 */
	class MapLabel extends JLabel implements MouseListener, MouseMotionListener, ComponentListener {
		
		private static final long serialVersionUID = 1L;
				
		private static final int MAP_WIDTH = 700;
		private static final int MAP_HEIGHT = 349;
		
		private static final int MAX_POINT_CLICK_DISTANCE = 7;
		private static final int POINT_RADIUS = 6;
		
		private List<ShapeCoords> shapes_;
		
		private ShapeCoords selectedShape_;
		private int selectedPointIndex_ = -1;
		private int selectedRightPoint1Index_ = -1;
		private int selectedRightPoint2Index_ = -1;
		
		private int startShapeCount = -1;
		
		private Color c_ = new Color(0.0f, 0.5f, 1.0f, 0.5f);
		
		/**
		 * Label containing map.
		 * 
		 */
		public MapLabel() {
			
			
			setHorizontalAlignment(SwingConstants.CENTER);
			
			// Add listeners
			addMouseListener(this);
			addMouseMotionListener(this);
			addComponentListener(this);
		}
		
		/**
		 * Main paint method. Draws all points, all shapes,
		 * also draws selected point
		 * 
		 * @param g
		 */
		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
						
			if(shapes_ != null) {
				for(int i = 0; i < shapes_.size(); i++) {
					
					ShapeCoords shapeCoords = shapes_.get(i);
					Shape shape = shapeCoords.getShape();		// tuto bude ShapeCoords.shape
					
					Graphics2D g2D = (Graphics2D) g;
					
		            if(shapeCoords.getType() != 2 && shapeCoords.getType() != 6) {
			            g2D.setPaint(c_);
			            g2D.fill(shape);
		            }
		            
		            g2D.setPaint(new Color(c_.getRGB()));
		            g2D.setStroke(new BasicStroke(3));
		            g2D.draw(shape);
		        
		            ///////////////////////////////////////////////////////
		            //					vykreslenie bodov				//
		            List<Point2D> points = shapes_.get(i).getPoints();
		            
		            for(int j = 0; j < points.size(); j++) {	
		            	g.setColor(c_);
		            	
		            	if(selectedShape_ != null) {
							if(selectedShape_.equals(shapeCoords) &&
									selectedRightPoint1Index_ == j) g.setColor(Color.BLUE);
		            	}
//						else if(j == shapeCoords.selectedRightPoint1Index_) g.setColor(Color.GREEN);
						
						
						Point2D point = points.get(j);
						
						double x = (((double)lMapContainer.getWidth() - 2*offset_X) / MAP_WIDTH * point.getX() + offset_X - POINT_RADIUS);
						double y = (((double)lMapContainer.getHeight() - 2*offset_Y) / MAP_HEIGHT * point.getY() + offset_Y - POINT_RADIUS);
						
						g.fillOval(	(int)x, 
									(int)y, 
									POINT_RADIUS * 2, 
									POINT_RADIUS * 2);
					}
					
				}
			}
		}
		
		

		/**
		 * Release listener when mouse is released.
		 * @param
		 */
		@Override
		public void mouseReleased(MouseEvent event) {
			if(this.getMouseMotionListeners().length == 0) {
				addMouseMotionListener(this);
			}
			
			if(selectedPointIndex_ != -1) {
				selectedPointIndex_ = -1;
			}
		}	

		@Override
		public void mouseClicked(MouseEvent arg0) {
			
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			
		}

		/**
		 * Calculate mouse position, add new point or drag selected
		 * @param event
		 */
		@Override
		public void mousePressed(MouseEvent event) {
			if(state.equals(States.IDLE)) {
				return;
			}
			
			if(SwingUtilities.isLeftMouseButton(event)) {
				MousePosition mPos;
				
				selectedRightPoint1Index_ = -1;
				selectedRightPoint2Index_ = -1;
				
				mPos = calculateLocalMousePosition(event.getX(), event.getY());
				
				if(mPos == null)	{	return;	}
				

				float minDistance = -1;
				for(int j = 0; j < shapes_.size(); j++) {
					List<Point2D> points = shapes_.get(j).getPoints();
					
					for(int i = 0; i < points.size(); i++) {
						Point2D point = points.get(i);
						
						float distance;
						
						double pointX = ((double)lMapContainer.getWidth() - 2*offset_X) / MAP_WIDTH * point.getX();
						double pointY = ((double)lMapContainer.getHeight() - 2*offset_Y) / MAP_HEIGHT * point.getY();
						
						distance = (float) Point2D.distance(mPos.getX(), mPos.getY(), (int)pointX, (int)pointY);
						
						if(distance < MAX_POINT_CLICK_DISTANCE && (distance < minDistance || minDistance == -1)) {
							minDistance = distance;
							selectedPointIndex_ = i;
							selectedShape_ = shapes_.get(j);
						}
					}	
				}
				// drag point or create new one
				if(selectedPointIndex_ == -1) {
					removeMouseMotionListener(this);
				}
			}
			else if(SwingUtilities.isRightMouseButton(event)) {
				if(shapes_ != null && shapes_.size() > 0 && shapes_.get(0).getType() == 1) {
					return;
				}
				
				MousePosition mPos;
				
				mPos = calculateLocalMousePosition(event.getX(), event.getY());
				
				if(mPos == null)	{	return;	}
				
				float minDistance = -1;
				
				// klikam prvy
				if(shapes_ != null) {
				if(selectedRightPoint1Index_ == -1) {
					for(int j = 0; j < shapes_.size(); j++) {
						List<Point2D> points = shapes_.get(j).getPoints();
						
						for(int i = 0; i < points.size(); i++) {
							Point2D point = points.get(i);
							
							float distance;
							
							double pointX = ((double)lMapContainer.getWidth() - 2*offset_X) / MAP_WIDTH * point.getX();
							double pointY = ((double)lMapContainer.getHeight() - 2*offset_Y) / MAP_HEIGHT * point.getY();
							
							distance = (float) Point2D.distance(mPos.getX(), mPos.getY(), (int)pointX, (int)pointY);
							
							if(distance < MAX_POINT_CLICK_DISTANCE && (distance < minDistance || minDistance == -1)) {
								minDistance = distance;
								selectedRightPoint1Index_ = i;
								selectedShape_ = shapes_.get(j);
							}
						}
					}
				}
				// klikam druhy
				else {
					List<Point2D> points = selectedShape_.getPoints();
					
					for(int i = 0; i < points.size(); i++) {
						Point2D point = points.get(i);
						
						float distance;
						
						double pointX = ((double)lMapContainer.getWidth() - 2*offset_X) / MAP_WIDTH * point.getX();
						double pointY = ((double)lMapContainer.getHeight() - 2*offset_Y) / MAP_HEIGHT * point.getY();
						
						distance = (float) Point2D.distance(mPos.getX(), mPos.getY(), (int)pointX, (int)pointY);
						
						if(distance < MAX_POINT_CLICK_DISTANCE && (distance < minDistance || minDistance == -1)) {
							minDistance = distance;
							selectedRightPoint2Index_ = i;
						}
					}
				}
				}
				
				revalidate();
				repaint();
				
				if(selectedRightPoint2Index_ != -1) {
					if(shapes_ != null && shapes_.size() > 0 && shapes_.get(0).getType() == 5) {
						return;
					}
					// Handle delete point or add new
					if((selectedRightPoint2Index_ - 1) % selectedShape_.getPoints().size() == selectedRightPoint1Index_
							|| (selectedRightPoint2Index_ + 1) % selectedShape_.getPoints().size() == selectedRightPoint1Index_
							|| (selectedRightPoint2Index_ == 0 && selectedRightPoint1Index_ == selectedShape_.getPoints().size() - 1)) {
						// pridaj novy medzi
						int newIndex = (selectedRightPoint2Index_ > selectedRightPoint1Index_)?
								selectedRightPoint2Index_ : selectedRightPoint1Index_;
						int lowerIndex = (selectedRightPoint2Index_ < selectedRightPoint1Index_)?
								selectedRightPoint2Index_ : selectedRightPoint1Index_;
						
						Point2D newPoint = (Point2D)selectedShape_.getPoints().get(newIndex).clone();
						
						newPoint.setLocation((newPoint.getX() + selectedShape_.getPoints().get(lowerIndex).getX()) / 2,
								(newPoint.getY() + selectedShape_.getPoints().get(lowerIndex).getY()) / 2);
						selectedShape_.getPoints().add(newIndex, newPoint);
						
						selectedRightPoint1Index_ = -1;
						selectedRightPoint2Index_ = -1;
						
						refreshMap();
					}
					else if(selectedRightPoint2Index_ == selectedRightPoint1Index_) {
						selectedShape_.getPoints().remove(selectedRightPoint2Index_);
						
						selectedRightPoint1Index_ = -1;
						selectedRightPoint2Index_ = -1;
						
						refreshMap();
					}
					else {
						selectedRightPoint1Index_ = -1;
						selectedRightPoint2Index_ = -1;
					}
				}
				else if(selectedRightPoint1Index_ == -1 && selectedRightPoint2Index_ == -1) {
					if(shapes_ != null && shapes_.size() > 0 && (shapes_.get(0).getType() == 2 || shapes_.get(0).getType() == 3)) {
						return;
					}
					
					// pridaj novy
					if(shapes_ == null)
						shapes_ = new ArrayList<ShapeCoords>();
					
					
					double mouseX = (mPos.getX()) / (((double)lMapContainer.getWidth() - 2*offset_X) / MAP_WIDTH);
					double mouseY = (mPos.getY()) / (((double)lMapContainer.getHeight() - 2*offset_Y) / MAP_HEIGHT);

					List<Point2D> points_ = new ArrayList<Point2D>();
					
					Point2D pnt;
					try {
						pnt = Point2D.Double.class.newInstance();
						
						pnt.setLocation(mouseX, mouseY);
						points_.add(pnt);
						
					} catch (InstantiationException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
								
					double[] a = new double[points_.size() * 2];
					
					for(int i = 0; i < points_.size(); i++) {
						Point2D point = points_.get(i);
						
						double x = (((double)lMapContainer.getWidth() - 2*offset_X) / MAP_WIDTH * point.getX() + offset_X);
						double y = (((double)lMapContainer.getHeight() - 2*offset_Y) / MAP_HEIGHT * point.getY() + offset_Y);
						
						a[i * 2] = x;
						a[i * 2 + 1] = y;
					}
					
					ShapeCoords sc1;
					
					if(shapes_.size() > 0) {
						JGeometry jg = new JGeometry(shapes_.get(0).getType(), 0, shapes_.get(0).getElemInfo(), a);
						
						sc1 = new ShapeCoords(jg.createShape(), shapes_.get(0).getType(), points_, shapes_.get(0).getElemInfo());
					}
					else {
						int t = -1;
						int[] elem = new int[]{};
						
						switch(shapeTypesComboBox.getSelectedIndex()){
						case 0:
							t = 3;
							elem = new int[]{1, 1003, 1};
							break;
						case 1:
							t = 7;	
							elem = new int[]{1, 1003, 1};
							break;
						case 2:
							t = 1;
							elem = new int[]{1, 1, 1};
							break;
						case 3:
							t = 5;
							elem = new int[]{1, 1, 1};
							break;
						case 4:
							t = 2;
							elem = new int[]{1, 2, 1};
							break;
						case 5:
							t = 6;
							elem = new int[]{1, 2, 1};
							break;
						}
						
						JGeometry jg = new JGeometry(t, 0, elem, a);
						
						sc1 = new ShapeCoords(jg.createShape(), t, points_, elem);
					}	

					shapes_.add(sc1);
					
				}
			}
			else if(SwingUtilities.isMiddleMouseButton(event)) {
				MousePosition mPos;
				int indexToRemove = -1;
				
				mPos = calculateLocalMousePosition(event.getX(), event.getY());
				
				if(mPos == null)	{	return;	}
				
				float minDistance = -1;
				
				for(int j = 0; j < shapes_.size(); j++) {
					List<Point2D> points = shapes_.get(j).getPoints();
					
					for(int i = 0; i < points.size(); i++) {
						Point2D point = points.get(i);
						
						float distance;
						
						double pointX = ((double)lMapContainer.getWidth() - 2*offset_X) / MAP_WIDTH * point.getX();
						double pointY = ((double)lMapContainer.getHeight() - 2*offset_Y) / MAP_HEIGHT * point.getY();
						
						distance = (float) Point2D.distance(mPos.getX(), mPos.getY(), (int)pointX, (int)pointY);
						
						if(distance < MAX_POINT_CLICK_DISTANCE && (distance < minDistance || minDistance == -1)) {
							minDistance = distance;
							indexToRemove = i;
							selectedShape_ = shapes_.get(j);

						}
					}
				}
				
				if(indexToRemove != -1) {
					selectedShape_.getPoints().remove(indexToRemove);
					if(selectedShape_.getPoints().isEmpty())
						shapes_.remove(selectedShape_);
					
					selectedShape_ = null;
					refreshMap();
				}
				
			}
			
		}
		
		@Override
		public void mouseDragged(MouseEvent event) {
			MousePosition mPos;
			
			mPos = calculateLocalMousePosition(event.getX(), event.getY());
			
			if(mPos == null || selectedPointIndex_ == -1) {
				mouseReleased(event);
				return;	
			}
			
			
			Point2D p = selectedShape_.getPoints().get(selectedPointIndex_);	
			

			double mouseX = (mPos.getX()) / (((double)lMapContainer.getWidth() - 2*offset_X) / MAP_WIDTH);
			double mouseY = (mPos.getY()) / (((double)lMapContainer.getHeight() - 2*offset_Y) / MAP_HEIGHT);

			
			p.setLocation(mouseX, mouseY);
			selectedShape_.getPoints().remove(selectedPointIndex_);
			selectedShape_.getPoints().add(selectedPointIndex_, p);
			

			
			refreshMap();
			

			revalidate();
			repaint();
		}

		@Override
		public void mouseMoved(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void componentHidden(ComponentEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void componentMoved(ComponentEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void componentResized(ComponentEvent e) {
			if(shapes_ != null)	{
				//normalizeShape();
				refreshMap();
			}
		}

		@Override
		public void componentShown(ComponentEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		//////////////////////////////////////////////////////////////////
		//				TRIEDNE METODY									//
		//////////////////////////////////////////////////////////////////
		/**
		 * Set color of polygon to be drawn.
		 * Colors are selected randomly
		 * @param c
		 */
		public void setColor(Color c){
			c_ = c;
		}
		
		/**
		 * Convert shapes of selected animal to the new type
		 * @param type
		 */
		public void changeShapeType(int type) {
			if(shapes_ == null || shapes_.size() == 0)
				return;
			
			int t = -1;
			int[] elem = null;
			
			switch(type){
			case 0:
				t = 3;
				elem = new int[]{1, 1003, 1};
				break;
			case 1:
				t = 7;	
				elem = new int[]{1, 1003, 1};
				break;
			case 2:
				t = 1;
				elem = new int[]{1, 1, 1};
				break;
			case 3:
				t = 5;
				elem = new int[]{1, 1, 1};
				break;
			case 4:
				t = 2;
				elem = new int[]{1, 2, 1};
				break;
			case 5:
				t = 6;
				elem = new int[]{1, 2, 1};
				break;
			}
			
			if(t == 1) {
				Point2D p = shapes_.get(0).getPoints().get(0);
				List<Point2D> l = new ArrayList<Point2D>();
				l.add(p);
				
				shapes_.get(0).setPoints(l);
				
				List<ShapeCoords> sc = new ArrayList<ShapeCoords>();
				sc.add(shapes_.get(0));
				
				shapes_ = sc;
			}
			else if(t == 5) {
				for(int i = 0; i < shapes_.size(); i++) {
					Point2D p = shapes_.get(i).getPoints().get(0);
					List<Point2D> l = new ArrayList<Point2D>();
					l.add(p);
					
					shapes_.get(i).setPoints(l);
				}
				
			}
			else if(t == 2 || t == 3) {
				List<ShapeCoords> sc = new ArrayList<ShapeCoords>();
				sc.add(shapes_.get(0));
				
				shapes_ = sc;
			}
			
			
			for(int i = 0; i < shapes_.size(); i++){
				
				shapes_.get(i).setType(t);
				shapes_.get(i).setElemInfo(elem);
		
				
				ShapeCoords shapeCoords = shapes_.get(i);
				
				double[] a = new double[shapeCoords.getPoints().size() * 2];
				
				for(int j = 0; j < shapeCoords.getPoints().size(); j++) {
					Point2D point = shapeCoords.getPoints().get(j);
					
					double x = (((double)lMapContainer.getWidth() - 2*offset_X) / MAP_WIDTH * point.getX() + offset_X);
					double y = (((double)lMapContainer.getHeight() - 2*offset_Y) / MAP_HEIGHT * point.getY() + offset_Y);
					
					a[j * 2] = x;
					a[j * 2 + 1] = y;
				}
				
				JGeometry jg = new JGeometry(shapes_.get(i).getType(), 0, shapes_.get(i).getElemInfo(), a);
	
				shapes_.get(i).setShape(jg.createShape());
			}
			refreshMap();
		}
		
		/**
		 * Clear map.
		 */
		public void clear() {
			if(shapes_ != null) {
				shapes_.clear();
				revalidate();
				repaint();
			}
		}
		
		/**
		 * Set shapes of selected animal on the map
		 * @param shapes
		 */
		public void setShapes(List<ShapeCoords> shapes) {
					
			shapes_ = shapes;
			
			if(shapes_ == null) {
				startShapeCount = 0;
			}
			else {
				startShapeCount = shapes_.size();
			}
						
			refreshMap();
		}
		
		/**
		 * Get count of shapes of selected animal before edit
		 * @return
		 */
		public int getStartShapeCount() {
			return startShapeCount;
		}
		
		/**
		 * Convert shape's coords from program coord system to db coord system
		 * @return shapes of selected animal 
		 */
		private List<ShapeCoords> normalizeShape() {
						
			List<ShapeCoords> newList = new ArrayList<ShapeCoords>();
			
			if(shapes_ == null || shapes_.isEmpty()) {	
				return null;
			}
			
			for(int j = 0; j < shapes_.size(); j++) {
				ShapeCoords shapeCoords = shapes_.get(j);
				
				double[] a = new double[shapeCoords.getPoints().size() * 2];
				
				for(int i = 0; i < shapeCoords.getPoints().size(); i++) {
					Point2D point = shapeCoords.getPoints().get(i);
					
					double x = point.getX();
					double y = point.getY();
					
					a[i * 2] = x;
					a[i * 2 + 1] = y;
				}
				
				JGeometry jg = new JGeometry(shapeCoords.getType(), 0, shapeCoords.getElemInfo(), a);
				shapeCoords.setShape(jg.createShape());
				
				newList.add(shapeCoords);
			}
			return newList;
		}

		/**
		 * Redraw all shapes
		 */
		private void refreshMap() {
			if(shapes_ == null) {
				
				revalidate();
				repaint();
				
				return;
			}
			
			for(int j = 0; j < shapes_.size(); j++) {
				ShapeCoords shapeCoords = shapes_.get(j);
				
				double[] a = new double[shapeCoords.getPoints().size() * 2];
				
				for(int i = 0; i < shapeCoords.getPoints().size(); i++) {
					Point2D point = shapeCoords.getPoints().get(i);
					
					double x = (((double)lMapContainer.getWidth() - 2*offset_X) / MAP_WIDTH * point.getX() + offset_X);
					double y = (((double)lMapContainer.getHeight() - 2*offset_Y) / MAP_HEIGHT * point.getY() + offset_Y);
					
					a[i * 2] = x;
					a[i * 2 + 1] = y;
				}
				
				JGeometry jg = new JGeometry(shapeCoords.getType(), 0, shapeCoords.getElemInfo(), a);
				shapeCoords.setShape(jg.createShape());
			}
			revalidate();
			repaint();
		}
		//////////////////////////////////////////////////////////////////
		//				POMOCNE FUNKCIE A TRIEDY						//
		//////////////////////////////////////////////////////////////////
	    
		/**
		 * Calculate mouse position above ICON of map label. 
		 * NOTE: Icon is not the same size as its parent label
		 * @param mouseX
		 * @param mouseY
		 * @return	local mouse position
		 */
		private MousePosition calculateLocalMousePosition(int mouseX, int mouseY) {
			int x, y, w, h;
			
			x = mouseX - offset_X;
			y = mouseY - offset_Y;
			
			w = lMapContainer.getWidth() - offset_X * 2;
			h = lMapContainer.getHeight() - offset_Y * 2;
									
			if(	x >= 0 && x < w && y >= 0 && y < h	) {
				return new MousePosition(x, y);
			}
			else {
				return null;
			}
		}

		/**
		 * Position class
		 * @author Roman
		 *
		 */
		class MousePosition {
			
			private int x_;
			private int y_;
			
			public MousePosition(int x, int y) {
				x_ = x;
				y_ = y;
			}
			
			public int getX() {	return x_;	}
			public int getY() {	return y_;	}
		}
	}
	
	/**
	 * Handle image preview click, choose new image, scale it and preview
	 */
	@Override
	public void mouseClicked(MouseEvent event) {
		Object source = event.getSource();
		
		if(source == tDImagePreview) {
			Dimension scaledIconDimension;
			Image image = null;
			Image newImg;
			int returnVal;
			JFileChooser fileChooser;
			ImageIcon scaledIcon;			
			
			fileChooser = new JFileChooser();
			fileChooser.setDialogTitle("Choose photo");
			
			returnVal = fileChooser.showDialog(this, "Choose..");
						         
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				newAnimalIcon = fileChooser.getSelectedFile();
			}
						
			if(newAnimalIcon == null)	return;
			
			try	{
			    image = ImageIO.read(newAnimalIcon);      
			} catch (IOException e) {
				e.printStackTrace();
			}			
			
			scaledIconDimension = getScaledDimension(	new Dimension(image.getWidth(null), image.getHeight(null)),
																new Dimension(tDImagePreview.getWidth(), tDImagePreview.getHeight())
																);
			
			newImg = image.getScaledInstance(	scaledIconDimension.width,
													scaledIconDimension.height, 
													java.awt.Image.SCALE_SMOOTH 
													);  
			scaledIcon = new ImageIcon( newImg );
			
			lDImagePreview.setIcon(scaledIcon);
			
			bDRotateImage.setEnabled(false);
		}
	}

	/**
	 * Change cursor above image preview element
	 */
	@Override
	public void mouseEntered(MouseEvent event) {
		Object source = event.getSource();
		
		if(source == tDImagePreview) {
			Cursor cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR); 
		    setCursor(cursor);
		}		
	}

	/**
	 * Change cursor to default if mouse left image preview panel
	 */
	@Override
	public void mouseExited(MouseEvent event) {
		Object source = event.getSource();
		
		if(source == tDImagePreview) {
			Cursor cursor = Cursor.getDefaultCursor();
		    setCursor(cursor);
		}
	}

	@Override
	public void mousePressed(MouseEvent event) {

	}

	@Override
	public void mouseReleased(MouseEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Handle resize program event.
	 * Resize and refresh map panel
	 */
	@Override
	public void componentResized(ComponentEvent event) {
		Object source = event.getSource();
		
		if(source == this) {
			initMap();
			lMapContainer.refreshMap();
		}
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
