package PDB15_projekt;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 * Lgin dialog window class
 * @author Roman
 *
 */
public class LoginDialog extends JDialog{
	
	private static final long serialVersionUID = 1L;
	
	private final JLabel lUserName = new JLabel("User Name");
	private final JLabel lPassword = new JLabel("Password");
	
	private final JTextField tUserName= new JTextField(8);
	private final JPasswordField tPassword= new JPasswordField();
	
	private JButton bLogin = new JButton("Login");
	private JButton bCancel = new JButton("Cancel");
	
	private final JLabel lStatus = new JLabel(" ");
	
	
	private final char[] pass1 = {'q', '9', 'h', 'm', '4', '2', 't', 'z'};
	private final char[] pass2 = {'j', '9', 'u', 'r', 's', 'a', 'r', 'j', 'o', 'n'};
	private final char[] pass3 = {'c', 'e', 'j', 'r', 'b', 'l', 'k', 'b'};
	
	public LoginDialog() {
		this(null, true);
	}
	
	/**
	 * Constructor - creates login dialog, implements listeners and functions
	 * @param parent
	 * @param modal
	 */
	public LoginDialog(final JFrame parent, boolean modal) {
		super(parent, modal);
		
		this.setTitle("Login");
		
		JPanel labelPanel = new JPanel(new GridLayout(2, 1));
		labelPanel.add(lUserName);
		labelPanel.add(lPassword);
		
		JPanel textPanel = new JPanel(new GridLayout(2, 1));
		textPanel.add(tUserName);
		textPanel.add(tPassword);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(bLogin);
		buttonPanel.add(bCancel);
		
		JPanel inputPanel = new JPanel();
		inputPanel.add(labelPanel);
		inputPanel.add(textPanel);
		
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(buttonPanel);
		panel.add(lStatus, BorderLayout.SOUTH);
		
		setLayout(new BorderLayout());
		add(inputPanel);
		add(panel, BorderLayout.SOUTH);
		pack();
		
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		addWindowListener(new WindowAdapter() {  
            @Override
            public void windowClosing(WindowEvent event) {  
                System.exit(0);  
            }  
        });
		
		bLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ((Arrays.equals(pass1, tPassword.getPassword())
                        && "xtabir00".equals(tUserName.getText())) ||
                		(Arrays.equals(pass2, tPassword.getPassword())
                                && "xvlkov01".equals(tUserName.getText()) ||
                                (Arrays.equals(pass3, tPassword.getPassword()))
                                        && "xkukli03".equals(tUserName.getText()))) {
                    parent.setVisible(true);
                    
                    Main main = (Main)parent;
                    main.connect(tUserName.getText());
                    
                    setVisible(false);
                } else {
                    lStatus.setText("Invalid username or password");
                }
            }
        });
        bCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                parent.dispose();
                System.exit(0);
            }
        });
	}
}
