package PDB15_projekt;

import java.awt.Shape;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
/**
 * class for geometry objects from db
 * @author Vladim�r Vlkovi� xvlkov01@stud.fit.vutbr.cz
 *
 */
public class ShapeCoords {
	/**
	 * shape of obejct
	 */
	private Shape shape;
	/**
	 * type of obejct
	 */
	private int type;
	/**
	 * points of obejct
	 */
	private List<Point2D> points;
	/**
	 * element info of obejct
	 */
	private int elemInfo[];
	/**
	 * 
	 * @param shape  shape of obejct
	 * @param type	 type of obejct
	 * @param points	 points of obejct
	 * @param elemInfo	 element info of obejct
	 */
	public ShapeCoords(Shape shape,int type,List<Point2D> points,int elemInfo[])
	{
		this.shape=shape;
		this.type=type;
		this.points=new ArrayList<Point2D>(points);
		this.elemInfo=elemInfo;
	}
	
	public Shape getShape()
	{
		return shape;
	}
	
	public int getType()
	{
		return type;
	}
	
	public List<Point2D> getPoints()
	{
		return points;
	}
	
	public int[] getElemInfo()
	{
		return elemInfo;
	}
	
	
	public void setShape(Shape shape)
	{
		this.shape=shape;
	}
	
	public void setType(int type)
	{
		this.type = type;
	}
	
	public void setPoints(List<Point2D> points)
	{
		this.points = points;
	}
	
	public void setElemInfo(int[] elemInfo)
	{
		this.elemInfo = elemInfo;
	}
}
