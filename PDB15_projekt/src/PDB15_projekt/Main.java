package PDB15_projekt;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.UIManager;

/**
 * Main class
 * @author Roman T�bi
 *
 */
public class Main extends JFrame{

	private static final long serialVersionUID = 1L;
	
	public static Database db = new Database();
	private static JFrame mainFrame;
	public static List<Integer> result ;
	public static List<Population> pop2 ;
	
	/**
	 * Login dialog window
	 */
	private LoginDialog loginDialog;
	
	/**
	 * Creates login dialog
	 */
	public Main() {
		loginDialog = new LoginDialog(this, true);
		loginDialog.setVisible(true);
	}
	
	/**
	 * Create and show GUI
	 */
	private static void createAndShowGUI(){
		mainFrame = new Main();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		mainFrame.setTitle("Animals");
		// Add content to the window
		mainFrame.add(new GUI(db));
		
		mainFrame.setSize(1450, 768);
	}
	
	/**
	 * Logout from GUI and DB allowing other user to log in
	 */
	public static void Logout() {
		mainFrame.dispose();
		mainFrame = null;
		
		try {
			main(null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Connect do database
	 * @param name	- user name
	 */
	public void connect(String name) {
		result = new LinkedList<Integer>();
		
		try {
			if(name.equals("xtabir00")){
				db.connect(name, "q9hm42tz");
			}
			else if(name.equals("xvlkov01")){
				db.connect(name, "j9ursarjon");
			}
			else if(name.equals("xkukli03")){
				db.connect(name, "cejrblkb");
			}
		
		
			//db.deleteTables();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}
	
	/**
	 * Main function of program
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException{
		
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				
				UIManager.put("swing.boldMetal", Boolean.FALSE);
				createAndShowGUI();
			}			
		});
		
	}

}
