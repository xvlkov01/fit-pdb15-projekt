package PDB15_projekt;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
/**
 * 
 *Class containing information about animals
 * @author Vladim�r Vlkovic xvlkov01@stud.fit.vutbr.cz
 */
public class Animal {
	/**
	 * Unique id
	 */
	private int id;
	/**
	 * genus of animal
	 */
	private String genusName;
	
	/**
	 * species of animal
	 */
	private String speciesName;
	
	/**
	 * order of animal
	 */
	private String order;
	
	/**
	 * if the animal is law protected
	 */
	private boolean lawProtection;
	
	/**
	 * List of all populations in certain time periods
	 */
	
	private List<Population> population;
	
	/**
	 * brief description of the animal
	 */
	private String description;
	
	
	/**
	 * Constructor
	 * @param id idetification number of animal
	 * @param genusName genus of animal
	 * @param speciesName species of animal
	 * @param order order of animal
	 * @param population List of populations of animal
	 * @param lawProtection law protection of animal
	 * @param description description of animal
	 */
	
	public Animal(int id, String genusName, String speciesName, String order, List<Population> population, boolean lawProtection,
			String description){
		
		this.id = id;
		this.genusName = genusName;
		this.speciesName = speciesName;
		this.order = order;
		this.lawProtection = lawProtection;
		this.population= new ArrayList<Population>(population);
		this.description = description;
		
		
	}
	

	public int getId()
	{
		return id;
	}
	
	public String getGenusName()
	{
		return genusName;
	}
	
	public String getSpeciesName()
	{
		return speciesName;
	}
	
	public String getOrder()
	{
		return order;
	}
	
	public boolean getLawProtection()
	{
		return lawProtection;
	}
	
	public List<Population> getPopulation()
	{
		return population;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	
	
	
	
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public void setGenusName(String genusName)
	{
		this.genusName = genusName;
	}
	
	public void setSpeciesName(String speciesName)
	{
		this.speciesName = speciesName;
	}
	
	public void setOrder(String order)
	{
		this.order = order;
	}
	
	public void setLawProtection(boolean lawProtection)
	{
		this.lawProtection = lawProtection;
	}
	
	public void setPopulation(List<Population> population)
	{
		this.population = population;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}
	
}
