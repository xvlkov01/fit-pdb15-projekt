package PDB15_projekt;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * Class containing information about animal population
 * @author Vladim�r Vlkovic xvlkov01@stud.fit.vutbr.cz
 *
 */
public class Population {
	/**
	 * number of current population
	 */
	private int population;
	/**
	 * validity of population from a certain date
	 */
	private Date validFrom;
	/**
	 * validity of population to a certain date
	 */
	private Date validTo ;
	
	/**
	 * Constructor
	 * @param population number of population
	 * @param validFrom validity from
	 * @param validTo validity to
	 */
	public Population(int population, Date validFrom, Date validTo)
	{
		this.population=population;
		this.validFrom=validFrom;
		this.validTo=validTo;
	}
	
	public int getPopulation()
	{
		return population;
	}
	
	public Date getValidFrom()
	{
		return validFrom;
	}
	
	public Date getValidTo()
	{
		return validTo;
	}
	
	
	
	public void setPopulation(int population)
	{
		this.population=population;
	}
	
	public void setValidFrom(Date validFrom)
	{
		this.validFrom=validFrom;
	}
	
	public void setValidTo(Date validTo)
	{
		this.validFrom=validTo;
	}

}
