package PDB15_projekt;

import java.awt.Shape;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.pool.OracleDataSource;
import oracle.ord.im.OrdImage;
import oracle.spatial.geometry.JGeometry;

/**
 * Library for database operations
 * @author Vladim�r Vlkovi� xvlkov01@stud.fit.vutbr.cz
 * @author Filip Kukli� xkukli03@stud.fit.vutbr.cz
 *
 */
public class Database { 
	
	/**
	 * Maximum String length for names
	 */
	public static final int MAX_NAME_STRING = 20;
	/**
	 * Maximum String length for description
	 */
	public static final int MAX_DESC_STRING = 500;
	/**
	 * List of Animals for storing search results and query results
	 */
	public List<Animal> animals;
	/**
	 * List of populations for storing search results and query results
	 */
	public List<Population> pop;
  
  public List<ShapeCoords> shapeCoords;
	
	
	
	private OracleDataSource ods;
	public Connection conn = null;
	public Database()
	 {
		 this.animals = new ArrayList<Animal>();
		 this.pop = new ArrayList<Population>();
     this.shapeCoords = new ArrayList<ShapeCoords>();
		 
	 }
	 
	 
	  
	  /** Connect to database
	   * 
	   * @param login 		user name
	   * @param password	user password
	   * @throws SQLException
	   */
	 
	 public void connect(String login, String password) throws SQLException
	 {
		 ods = new OracleDataSource();
		 ods.setURL("jdbc:oracle:thin:@gort.fit.vutbr.cz:1521:dbgort");
		 ods.setUser(login);
		 ods.setPassword(password);
		 conn = ods.getConnection();
		 conn.setAutoCommit(true);
	 }
	 
	 
	 /**
	  * Creates tables in database
	  * @throws SQLException
	  */
	 
	 public  void createTables() throws SQLException
	 {
		 deleteTables();
		 Statement stmt = conn.createStatement();
		 try{
		 stmt.execute("CREATE TABLE ANIMALS "
		 				+ "(idAnimal INTEGER PRIMARY KEY  ,"
		 				+ "genusName VARCHAR(" + MAX_NAME_STRING +"),"
		 				+ "speciesName VARCHAR(" + MAX_NAME_STRING +"),"
		 				+ "orderName VARCHAR(" + MAX_NAME_STRING +"),"
		 				+ "lawProtection NUMBER(1) NOT NULL CHECK (lawProtection in (0,1)),"
		 				+ "description VARCHAR(" + MAX_DESC_STRING +"))");
		 
		
		 
		
		  stmt.execute("CREATE TABLE POPULATION "
			 		+ "(idPop INTEGER PRIMARY KEY, "
			 		+ "idAnimal REFERENCES ANIMALS(idAnimal) ON DELETE CASCADE,"
			 		+ "population INTEGER, "
			 		+ "validFrom DATE,"
			 		+ "validTo DATE)");
		 
		 stmt.execute("CREATE TABLE PHOTO "
				 + "(idPhoto INTEGER PRIMARY KEY ,"
				 + "idAnimal REFERENCES ANIMALS(idAnimal) ON DELETE CASCADE,"
				 + "photo ORDSYS.ORDImage,"
				 + "photoSI ORDSYS.SI_StillImage,"
				 + "photoAC ORDSYS.SI_AverageColor,"
				 + "photoCH ORDSYS.SI_ColorHistogram,"
				 + "photoPC ORDSYS.SI_PositionalColor,"
				 + "photoTX ORDSYS.SI_Texture)");
		 
		
		 stmt.execute("CREATE TABLE HABITAT  "
		 		+ "(idHabitat INTEGER PRIMARY KEY ,"
		 		+ "idAnimal REFERENCES ANIMALS(idAnimal) ON DELETE CASCADE,"
		 		+ "geometry SDO_GEOMETRY)");
		 
		 stmt.execute("CREATE INDEX habitatIdx ON habitat (geometry) indextype is MDSYS.SPATIAL_INDEX");
		 
		 }catch(SQLException e)
		 	{
			 	e.printStackTrace();
		 	}
		 
		
		 try
		 {
			 stmt.execute("INSERT INTO USER_SDO_GEOM_METADATA VALUES ('habitat', 'geometry',SDO_DIM_ARRAY(SDO_DIM_ELEMENT('X', 0, 700, 0.9), SDO_DIM_ELEMENT('Y', 0, 349, 0.9)),NULL)");
		 }catch(SQLException e){}
		 
		
		 createSequence();
		 createTrigger();
		 
		 
		 ////tento to tu musi byt kvoli vyhladavaniu podal obrazkov
		 stmt.executeQuery("INSERT INTO animals(genusname,speciesname,ordername,lawprotection,description)"
		 		+ "VALUES('kvoli','obr','tu',0,'je tento insert')");
	
		 stmt.close();
	 }
	 
	 
     /**
      * Loads data from a text file and pictures from a example folder to database        
      * @throws SQLException
      * @throws FileNotFoundException
      * @throws IOException
      */
     public void addAnimals() throws SQLException, FileNotFoundException, IOException
	 {
		Statement stmt = conn.createStatement();
        
		
		BufferedReader br = new BufferedReader(new InputStreamReader(Main.class.getResource("/animals.txt").openStream())); 
               
                    for(String line; (line = br.readLine()) != null; ) 
                    {
                   
                            stmt.executeQuery(line);
                    }
   
        
         
      
		for(int i=2; i<19; i++)
		{
			
			loadPhotoFromFile(i,"/"+i+".jpg");

		}
		 stmt.close();
		
	 }
         
	 
	 /**
	  * Deletes tables,sequences and triggers
	  */
	 public void deleteTables() 
	 {
		 Statement stmt;
		
		 try{
			 stmt = conn.createStatement();
			
			 stmt.execute("DROP TABLE HABITAT");

			 stmt.execute("DROP TABLE PHOTO");
			 
			 stmt.execute("DROP TABLE POPULATION");

			 stmt.execute("DROP TABLE ANIMALS");
			 
			 stmt.execute("DROP SEQUENCE animalsSeq");
			 
			 stmt.execute("DROP SEQUENCE photoSeq");
			 
			 stmt.execute("DROP SEQUENCE habitatSeq");
	
			 stmt.execute("DROP SEQUENCE popSeq");

			 stmt.execute("DROP TRIGGER animalsTrigg");

			 stmt.execute("DROP TRIGGER habitatTrigg");
			 stmt.execute("DROP INDEX habitatIdx");
			 stmt.execute("DROP TRIGGER popTrigg");
			 
			 stmt.close();
			 conn.commit();
		}catch(SQLException e)
		{ }
			
		
		 
	 }
	 
	 /**
	  * Creates sequences for id auto increment
	  * @throws SQLException
	  */
	 public void createSequence() throws SQLException
	 {
		 Statement stmt = conn.createStatement();
		 stmt.execute("CREATE SEQUENCE animalsSeq START WITH 1 INCREMENT BY 1");
		 stmt.execute("CREATE SEQUENCE photoSeq START WITH 1 INCREMENT BY 1");
		 stmt.execute("CREATE SEQUENCE habitatSeq START WITH 1 INCREMENT BY 1");
		 stmt.execute("CREATE SEQUENCE popSeq START WITH 1 INCREMENT BY 1");
		 stmt.close();
		 
	 }
	 
	/**
	 * Creates triggers for id auto increment
	 * @throws SQLException
	 */
	 public void createTrigger() throws SQLException
	 {
		 Statement stmt = conn.createStatement();
		 stmt.execute("CREATE OR REPLACE TRIGGER animalsTrigg "
		 		+ "BEFORE INSERT ON animals FOR EACH ROW "
		 		+ "BEGIN "
		 		+ "SELECT animalsSeq.nextval "		 		
		 		+ "INTO :NEW.idAnimal " 
		 		+ "FROM dual;"
		 		+ "END; ");
		 
		 
		 stmt.execute("CREATE OR REPLACE TRIGGER habitatTrigg "
			 		+ "BEFORE INSERT ON habitat FOR EACH ROW "
			 		+ "BEGIN "
			 		+ "SELECT habitatSeq.nextval "
			 		+ "INTO :NEW.idHabitat "
			 		+ "FROM dual;"
			 		+ "END; ");
		
		 
		 stmt.execute("CREATE OR REPLACE TRIGGER popTrigg "
			 		+ "BEFORE INSERT ON population FOR EACH ROW "
			 		+ "BEGIN "
			 		+ "SELECT popSeq.nextval "
			 		+ "INTO :NEW.idPop "
			 		+ "FROM dual;"
			 		+ "END; ");
		 stmt.close();
	 }
	 
	 
	 /**
	  * Loads image to database from a file stored on disc
	  * @param idAnimal		id of an animal
	  * @param fileName		absolute path to the file					
	  * @throws SQLException
	  * @throws IOException
	  */
	 public void loadPhotoFromFile(int idAnimal, String fileName) throws SQLException, IOException
	 {
		 conn.setAutoCommit(false);
		 Statement stmt = conn.createStatement();
		 String query = "SELECT photoSeq.nextval FROM dual";
		 OrdImage imgProxy = null;
		 OracleResultSet rset =  (OracleResultSet) stmt.executeQuery(query);
		 rset.next();
		 int idNextValue = rset.getInt("nextval");
		 query="INSERT INTO photo(idPhoto,idAnimal,photo)"
				+ "VALUES("+idNextValue+ ", "+ idAnimal +",ordsys.ordimage.init())";
		 stmt.executeQuery(query);
		
		 query = "SELECT photo FROM PHOTO  WHERE idPhoto = " + Integer.toString(idNextValue)
				+ " FOR UPDATE";

		 rset = (OracleResultSet) stmt.executeQuery(query);
		 rset.next();
		 imgProxy = (OrdImage) rset.getORAData("photo", OrdImage.getORADataFactory());
		 rset.close();	
		 
//		 BufferedImage file = ImageIO.read(Main.class.getResource(fileName));
		 
		 URL url = Main.class.getResource(fileName);
		 if(url == null) {
			 imgProxy.loadDataFromFile(fileName);
		 }
		 else {
			 imgProxy.loadDataFromInputStream(url.openStream());
		 }
		 
		 
//		 imgProxy.loadDataFromFile(fileName);
		 imgProxy.setProperties();
   
		 OraclePreparedStatement pstmtUpdate = (OraclePreparedStatement) conn.prepareStatement("UPDATE PHOTO SET photo=?  WHERE idPhoto=?");
		 pstmtUpdate.setORAData(1, imgProxy);
		 pstmtUpdate.setInt(2, idNextValue);
		 pstmtUpdate.executeUpdate();
		 pstmtUpdate.close();
		 OraclePreparedStatement pstmtUpdate2 = (OraclePreparedStatement) conn.prepareStatement(
                 "UPDATE PHOTO p SET p.photoSI = SI_StillImage(p.photo.getContent()) "
                 + "WHERE idPhoto = ?");
         
         pstmtUpdate2.setInt(1, idNextValue);
         pstmtUpdate2.executeUpdate();
    
         pstmtUpdate2.close();
             
             
         OraclePreparedStatement pstmtUpdate3 = (OraclePreparedStatement) conn.prepareStatement(
                 "UPDATE PHOTO SET "
                 + "photoAC = SI_AverageColor(photoSI), "
                 + "photoCH = SI_ColorHistogram(photoSI), "
                 + "photoPC = SI_PositionalColor(photoSI), "
                 + "photoTX = SI_Texture(photoSI) "
                 + "WHERE idPhoto = ?");
        
         pstmtUpdate3.setInt(1, idNextValue);
         pstmtUpdate3.executeUpdate();
    
         pstmtUpdate3.close();
         conn.commit();
         conn.setAutoCommit(true);
         stmt.close();
		 	 
	 }
	 
	 
	 /**
	  * Loads image from database
	  * @param idAnimal		id of animal									
	  * @return				OrdImage of the image in the database of the desired animal
	  * @throws SQLException
	  * @throws IOException
	  */
	 public OrdImage savePhotoToFile(int idAnimal) throws SQLException, IOException
	 {
		 OrdImage imgProxy = null;
		 String query = "SELECT photo from PHOTO WHERE idAnimal = ?";
		 OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn
 				.prepareStatement(query);
		 try {
	            pstmtSelect.setInt(1, idAnimal);
	            OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery();
	            try {
	                if (rset.next()) {
	                    imgProxy = (OrdImage) rset.getORAData("photo", OrdImage.getORADataFactory());
	                }
	            } finally {
	                rset.close();
	            }
	        } finally {
	            pstmtSelect.close();
	        }
	        return imgProxy;
	        
	 }
	 
	/**
	 * Searches images and orders by the most similar
	 * @param fileName		absolute path of the file which will be compared to the images in db
	 * @return		List of Animals which similarity is below a treshold, ordered by most similar
	 * @throws SQLException
	 * @throws IOException
	 */
	 public List<Animal> searchByPhoto(String fileName) throws SQLException, IOException
	 {
		 this.animals.clear();
		
		 loadPhotoFromFile(1,fileName);
		 Statement stmt=conn.createStatement();
		 
		 OracleResultSet rset=  (OracleResultSet) stmt.executeQuery(
				 "SELECT an.idAnimal,an.genusName,an.speciesName,an.orderName,an.lawProtection,an.description, "
				+ "SI_ScoreByFtrList(new SI_FeatureList(src.photoAC,0.3,src.photoCH,0.4,src.photoPC,0.4,src.photoTX,0.2),dst.photoSI)"
			    + " as similarity FROM ANIMALS an, PHOTO src, PHOTO dst  "
			    + "WHERE (src.idPhoto <> dst.idPhoto) AND (src.idAnimal=1) AND dst.idAnimal=an.idAnimal"
			    + " ORDER BY similarity ASC");
		
		 while(rset.next())
		 {
			 if(rset.getInt(7)<=10)
			 {
				 this.animals.add(new Animal(rset.getInt(1), rset.getString(2), rset.getString(3),
					 rset.getString(4),getAnimalPopulation(rset.getInt(1)),rset.getBoolean(5),rset.getString(6)));
			 }
		 }
		
		 stmt.executeQuery("DELETE FROM PHOTO WHERE idAnimal="+1);
		 rset.close();
		 stmt.close();
		
		return this.animals;
	 }
	 
	
	 
	
	 
	 /**
	  * Rotates picture in database 
	  * @param idAnimal		id of animal
	  * @param rotAngle		image rotation angle
	  * @throws SQLException
	  */
	 public void pictureRotation(int idAnimal, float rotAngle) throws SQLException
	 {
		 Statement stmt = conn.createStatement();
		 String sRotAngle = Float.toString(rotAngle);
		 stmt.execute("DECLARE img ORDSYS.ORDImage;"
		 		+ "BEGIN"
		 		+ "  SELECT photo INTO  img FROM Photo WHERE idAnimal = "+Integer.toString(idAnimal)+" FOR UPDATE;"
		 		+ "ORDSYS.ORDImage.process(img, 'rotate="+ sRotAngle+"');"
		 				+ " img.setProperties;                          "
		 				+ "UPDATE PHOTO SET photo=img WHERE idAnimal ="+Integer.toString(idAnimal)+";"
		 				+ "COMMIT; "
		 				+ " END;");
	 }
	 
	/**
	 * Deletes animal from database
	 * @param idAnimal		id of Animal
	 * @throws SQLException
	 */
	 public void deleteAnimal(int idAnimal) throws SQLException
	 {
		 
		 OraclePreparedStatement pstmtDel = (OraclePreparedStatement) conn.prepareStatement("DELETE FROM animals WHERE idAnimal=?");
		 pstmtDel.setInt(1, idAnimal);
		 pstmtDel.execute();
		 pstmtDel.close();
		 
		 
	 }
	 /**
	  * Deletes photo from database
	  * @param idAnimal		id of animal
	  * @throws SQLException
	  */
	 public void deletePhoto(int idAnimal ) throws SQLException
	 {
		 OraclePreparedStatement pstmtDel = (OraclePreparedStatement) conn.prepareStatement("DELETE FROM photo WHERE idAnimal=? ");
		 pstmtDel.setInt(1, idAnimal);
		 pstmtDel.execute();
		 pstmtDel.close();
	 }
	 
	 /**
	  * Inserts a new animal in database
	  * @param genusName	genus of the inserted animal
	  * @param speciesName	species of the inserted animal
	  * @param orderName	order of the inserted animal
	  * @param lawProtection	if the animal is protected by law then true else if not then false
	  * @param description	a brief description of the inserted animal 
	  * @return		 id of inserted animal
	  * @throws SQLException
	  */
	 public int insertAnimal(String genusName, String speciesName, String orderName, 
			 boolean lawProtection, String description) throws SQLException
	 {
		 OraclePreparedStatement pstmtInsert = 
				 (OraclePreparedStatement) conn.prepareStatement("INSERT INTO animals "
				 		+ "(genusName, speciesName, orderName, lawProtection, description)"
				 		+ "VALUES(?,?,?,?,?)");
		 pstmtInsert.setString(1, genusName);
		 pstmtInsert.setString(2, speciesName);
		 pstmtInsert.setString(3, orderName);
		 pstmtInsert.setBoolean(4, lawProtection);
		 pstmtInsert.setString(5, description);
		 pstmtInsert.execute();
		 pstmtInsert.close();
		 int tmp=0;
		 if(lawProtection) tmp=1;
		 else tmp=0;
		 String query="SELECT idAnimal FROM animals WHERE lawprotection="+tmp;
		 if(!genusName.equals(""))
		 {
			 query=query+ " AND genusName='"+genusName+"' ";
		 }
		 if(!speciesName.equals(""))
		 {
			 query=query+ " AND speciesName='"+speciesName+"' ";
		 }
		 if(!orderName.equals(""))
		 {
			 query=query+ " AND orderName='"+orderName+"' ";
		 }
		 if(!description.equals(""))
		 {
			 query=query+ "AND description='"+description+"'";
		 }
		 
		 Statement stmt=conn.createStatement();
		 ResultSet rset=stmt.executeQuery(query);
		
		 while(rset.next())
		 {
			 return rset.getInt(1);
		 }
		 
		 return 0;
	 }
	 

	 
	 /**
	  * Updates an existing animal in database
	  *	@param genusName	genus of the inserted animal
	  * @param speciesName	species of the inserted animal
	  * @param orderName	order of the inserted animal
	  * @param lawProtection	if the animal is protected by law then true else if not then false
	  * @param description	a brief description of the inserted animal 
	  * @throws SQLException
	  */
	 public void updateAnimal(int idAnimal, String genusName, String speciesName, String orderName, 
			 boolean lawProtection, String description) throws SQLException
	 {
		 
		 OraclePreparedStatement pstmtUpdate = 
				 (OraclePreparedStatement) conn.prepareStatement("UPDATE animals "
				 		+ "SET genusName=?, speciesName=?, orderName=?, lawProtection=?, description=? "
				 		+ "WHERE idAnimal=?");
		 pstmtUpdate.setString(1, genusName);
		 pstmtUpdate.setString(2, speciesName);
		 pstmtUpdate.setString(3, orderName);
		 pstmtUpdate.setBoolean(4, lawProtection);
		 pstmtUpdate.setString(5, description);
		 pstmtUpdate.setInt(6, idAnimal);
		 pstmtUpdate.execute();
		 pstmtUpdate.close();
		 
		 	 
	 }
	/**
	 * Gets all of the populations from database from a certain animal
	 * @param idAnimal id fo animal
	 * @return List of populations of the desired animal
	 * @throws SQLException
	 */
	 public List<Population> getAnimalPopulation(int idAnimal) throws SQLException
	 {
		 Statement stmt=conn.createStatement();
		 ResultSet rset=stmt.executeQuery("SELECT population,validFrom,validTo FROM population "
		 		+ "WHERE idAnimal="+idAnimal+" ORDER BY validFrom ASC");
		 this.pop.clear();
		 while(rset.next())
		 {
			 this.pop.add(new Population(rset.getInt(1),rset.getDate(2),rset.getDate(3)));
		 }
		 return this.pop;
	 }
	 
	 /**
	  * Gets all animals stored in database 
	  * @return List of all animals stored in the database
	  * @throws SQLException
	  */
	 public List<Animal> loadAnimalsFromDB() 
	 {
		 try {
		 int id=0;
		 Statement stmt = conn.createStatement();
		 ResultSet rset = stmt.executeQuery("SELECT * FROM animals WHERE idAnimal!=1");
		 this.animals.clear();
		 while(rset.next())
		 {
			
			 id=rset.getInt(1);
			 this.animals.add(new Animal(id, rset.getString(2), rset.getString(3), rset.getString(4), getAnimalPopulation(id), rset.getBoolean(5), rset.getString(6)));
		 }
		 rset.close();
		 stmt.close();
		 }catch (Exception e) {
			 return null;
		 }
		 return this.animals;
	 }
	/**
	 * Devides multi objects into sigle ones
	 * @param jGeometry JGeometry of an object
	 * @param type type of object
	 */
	 public void multiPolygonDivision(JGeometry jGeometry, int type)
	 {
		 int elemInfo[];
		 double points[];
		 int polInfo[]={1,1,1};
		 List<Point2D> points2d = new ArrayList<Point2D>();
      	 points=jGeometry.getOrdinatesArray();
      	 elemInfo=jGeometry.getElemInfo();
      	 int borderCoords[]=new int[elemInfo.length/3]; ///lebo polygon je def 3 cislami pre pocet staci 1/3
      	 
      	 switch(type)
      	 {
      	 	case JGeometry.GTYPE_MULTICURVE:
      	 		polInfo[1]=2;
      	 		break;
      	 	case JGeometry.GTYPE_MULTIPOINT:
      	 		
      	 		break;
      	 	case JGeometry.GTYPE_MULTIPOLYGON:
      	 		polInfo[1]=1003;
      	 		break;
      	 }
      	 
      	 int bCIterator=0;
      	 for(int i=0;i<elemInfo.length;i=i+3)
      	 {	
      		borderCoords[bCIterator]=elemInfo[i]; ///ziskanie od ktorych suradnic ma zacinat polygon
      		bCIterator++;	
      	 }

      	 int j=0;
         int bCLenghtIterator=0;
         while(bCLenghtIterator<borderCoords.length)
         {
        	 double[] tmpPoints=null;
          	 if(bCLenghtIterator<borderCoords.length-1)  ///ak mam za aktualnym polygonom dalsi
          	 {
          		 tmpPoints=new double[borderCoords[bCLenghtIterator+1]-borderCoords[bCLenghtIterator]];
          		 int tmpPointsIterator=0;
          		 for( j=borderCoords[bCLenghtIterator]-1;j<borderCoords[bCLenghtIterator+1]-1;j++)     //j=zaciatok suradnic -1 j<ako zaciatok suradnic nasledujuceho polygonu-1
          		 {		
          			 tmpPoints[tmpPointsIterator]=points[j];           ///ziskanie suradnic polygonu pre vytvorenie geometrie
          			 tmpPointsIterator++;
          	    
          		 }
          		 int pi=0;
          		 points2d.clear();
          		 while(pi<tmpPoints.length)
                 {
                     points2d.add(new Point2D.Double(tmpPoints[pi],tmpPoints[pi+1])); ///vytvorenie Point2D pre triedu pi a pi+1 x a y su za sebou
                     pi=pi+2;
                 }
          	 }
          	 else   ///ak nemam za aktualnym polygonom dalsi
          	 {
          		int tmpPointsIterator=0;
          		j=borderCoords[bCLenghtIterator]-1;
          		tmpPoints=new double[points.length-borderCoords[bCLenghtIterator]+1];  ////velkost pola dlzka vsetkych bodov-zaciatok suradnic posledneho polygonu +1
          		while(j<points.length)  ///iterujem do konca
          			{
          				tmpPoints[tmpPointsIterator]=points[j];
          				tmpPointsIterator++;
              			j++;
              		}
          			int pi=0;                                            ////to co vyssie
          			points2d.clear();
             		while(pi<tmpPoints.length)
                    {
                     	points2d.add(new Point2D.Double(tmpPoints[pi],tmpPoints[pi+1]));
                     	pi=pi+2;
                     }
              		
          	 }
          	 
          	 JGeometry jg = new JGeometry(type, 0, polInfo, tmpPoints);
          	 this.shapeCoords.add(new ShapeCoords(jg.createShape(),jg.getType(),points2d,jg.getElemInfo()));
          	 bCLenghtIterator++;

      	}
	 }
	 /**
	  * Gets the habitat of a desired animal
	  * @param idAnimal id of animal
	  */
	    /**
	  * Gets the habitat of a desired animal
	  * @param idAnimal id of animal
	 * @throws Exception 
	  */
	 public List<ShapeCoords> getAnimalHabitat(int idAnimal) throws Exception
	 {
		 shapeCoords.clear();
		 
		 List<Point2D> points2d = new ArrayList<Point2D>();
		 Statement stmt = conn.createStatement();
		 ResultSet rset = stmt.executeQuery("select geometry from habitat where idAnimal="+idAnimal);
		 Shape shape=null;
		 int elemInfo[];
		 int type=-1;
		 double points[];
		 while(rset.next())
		 {
			 byte[] image = rset.getBytes("geometry");
             JGeometry jGeometry = JGeometry.load(image);
             type=jGeometry.getType();
             switch (type) {
             
             case JGeometry.GTYPE_POLYGON:
                 shape = jGeometry.createShape();
                 break;
             
             case JGeometry.GTYPE_MULTIPOLYGON:
            	 multiPolygonDivision(jGeometry,JGeometry.GTYPE_MULTIPOLYGON);
    
             	break;
             case JGeometry.GTYPE_POINT:
             	shape = jGeometry.createShape();
             	break;
             case JGeometry.GTYPE_MULTIPOINT:
            	 multiPolygonDivision(jGeometry,JGeometry.GTYPE_MULTIPOINT);
             	break;
             case JGeometry.GTYPE_MULTICURVE:
            	 multiPolygonDivision(jGeometry,JGeometry.GTYPE_MULTICURVE);
             	break;
             case JGeometry.GTYPE_CURVE:
             	shape = jGeometry.createShape();
             	break;
             	
             default:
            	 break;
             }
           
             if(!(jGeometry.getType()==JGeometry.GTYPE_MULTIPOLYGON || jGeometry.getType()==JGeometry.GTYPE_MULTIPOINT || jGeometry.getType()==JGeometry.GTYPE_MULTICURVE))
             {
            	 points=jGeometry.getOrdinatesArray();
                 int i=0;
                 points2d.clear();
                 while(i<points.length)
                 {
                 	
                 	points2d.add(new Point2D.Double(points[i],points[i+1]));
                 	i=i+2;
                 }
                 for(Iterator<Point2D> j=points2d.iterator();j.hasNext();)
                 {
                 	Point2D temp=j.next();
                 	
                 }
                 elemInfo=jGeometry.getElemInfo();
                this.shapeCoords.add(new ShapeCoords(shape,type,points2d,elemInfo));
             }
           
         }
		
		
		 return this.shapeCoords;
	 }
	 /**
	  * Creates query for habitat insertion
	  * @param geometry
	  * @return
	  */
	 public String createHabitatQuery(List<ShapeCoords> geometry)
	 {
		 String query="";
		 if(geometry.size()==1)
		 {
			 int elemInfo []=geometry.get(0).getElemInfo();
			 switch(geometry.get(0).getType())
			 {
			 	case JGeometry.GTYPE_POLYGON:
			 		query=query+"SDO_GEOMETRY(2003, NULL, NULL,SDO_ELEM_INFO_ARRAY("+elemInfo[0]+","+elemInfo[1]+","+elemInfo[2]+"),"
				 		+ "SDO_ORDINATE_ARRAY(";
	
			 		break;
			 	case JGeometry.GTYPE_MULTIPOLYGON:
			 		query=query+"SDO_GEOMETRY(2007, NULL, NULL,SDO_ELEM_INFO_ARRAY("+elemInfo[0]+","+elemInfo[1]+","+elemInfo[2]+"),"
					 		+ "SDO_ORDINATE_ARRAY(";
				 	
				 		break;
			 		
          
			 	case JGeometry.GTYPE_POINT:
			 		query=query+"SDO_GEOMETRY(2001, NULL, NULL,SDO_ELEM_INFO_ARRAY("+elemInfo[0]+","+elemInfo[1]+","+elemInfo[2]+"),"
 				 		+ "SDO_ORDINATE_ARRAY(";

			 		break;
			 		
			 	case JGeometry.GTYPE_MULTIPOINT:
			 	query=query+"SDO_GEOMETRY(2005, NULL, NULL,SDO_ELEM_INFO_ARRAY("+elemInfo[0]+","+elemInfo[1]+","+elemInfo[2]+"),"
 				 		+ "SDO_ORDINATE_ARRAY(";
	
			 		break;
			 	
			 	case JGeometry.GTYPE_CURVE:
			 		query=query+"SDO_GEOMETRY(2002, NULL, NULL,SDO_ELEM_INFO_ARRAY("+elemInfo[0]+","+elemInfo[1]+","+elemInfo[2]+"),"
   				 		+ "SDO_ORDINATE_ARRAY(";
			 	
	   				 break;
	   				 
			 	case JGeometry.GTYPE_MULTICURVE:
			 		query=query+"SDO_GEOMETRY(2006, NULL, NULL,SDO_ELEM_INFO_ARRAY("+elemInfo[0]+","+elemInfo[1]+","+elemInfo[2]+"),"
	   				 		+ "SDO_ORDINATE_ARRAY(";
				 		
		             
		   				 break;
             	
			 	default:
			 		break;
			 }
			 for(Iterator<Point2D> f=geometry.get(0).getPoints().iterator();f.hasNext();)
	           	{
	           		Point2D p=f.next();
	           		query=query+""+p.getX()+","+p.getY()+",";
	           		
	           	}
				 query=query.substring(0,query.length()-1);
				 query=query+")))";
			
		 }
		 else
		 {
			 switch(geometry.get(0).getType())
			 {
			 case JGeometry.GTYPE_MULTIPOINT:
				 query=query+"SDO_GEOMETRY(2005, NULL, NULL,SDO_ELEM_INFO_ARRAY(";
			 		break;
			 	case JGeometry.GTYPE_MULTICURVE:
			 		 query=query+"SDO_GEOMETRY(2006, NULL, NULL,SDO_ELEM_INFO_ARRAY(";
         	 
          	
			 		break;
			 	case JGeometry.GTYPE_MULTIPOLYGON:
		         	 
			 		 query=query+"SDO_GEOMETRY(2007, NULL, NULL,SDO_ELEM_INFO_ARRAY(";
			 		break;
			 }
			
			 int count=1;
			 int bc=0;
			 int cc=1;
			 int beginCoords [] = new int[geometry.size()-1];
			 for(Iterator<ShapeCoords> j=geometry.iterator();j.hasNext();)
	         {
				 ShapeCoords temp=j.next();
				 for(Iterator<Point2D> f=temp.getPoints().iterator();f.hasNext();)
		         {
					 f.next();
					 count+=2;
		         }
				 
				 if(cc<geometry.size())
				 {
					
					 beginCoords[bc]=count;
					 bc++;
				 }
				 
				 cc++;
	           }
			 
			 int flag=0;
			
			 bc=0;
			 for(Iterator<ShapeCoords> j=geometry.iterator();j.hasNext();)
	         {
				
				 ShapeCoords temp=j.next();
				 if(flag==0)
				 {
					 query=query+""+temp.getElemInfo()[0]+","+temp.getElemInfo()[1]+","+temp.getElemInfo()[2]+",";
					
					 flag=1;
				 }
				 else
				 {
	
					 query=query+""+beginCoords[bc]+","+temp.getElemInfo()[1]+","+temp.getElemInfo()[2]+",";
					 bc++;
				 }
	          
	         }
			 query=query.substring(0,query.length()-1);
			 query=query+ "),SDO_ORDINATE_ARRAY(";
			 for(Iterator<ShapeCoords> j=geometry.iterator();j.hasNext();)
	         {
				 ShapeCoords temp=j.next();
				 for(Iterator<Point2D> f=temp.getPoints().iterator();f.hasNext();)
			     {
					 Point2D p=f.next();
			         query=query+""+p.getX()+","+p.getY()+",";
			     }
					 
	         }
			 query=query.substring(0,query.length()-1);
			 query=query+")))";
			 
		 }
		 return query;
	 }
	 
	
	 
	/**
	 * Insert a new habitat for desired animal
	 * @param idAnimal id of animal
	 * @param geometry habitat
	 * @throws SQLException
	 */
	 public void insertAnimalHabitat(int idAnimal, List<ShapeCoords> geometry) throws SQLException
	 {
		 Statement stmt=conn.createStatement();
		 
		 String query="INSERT INTO habitat(idAnimal,geometry) VALUES ("+idAnimal+",";
		query=query + createHabitatQuery(geometry);
		 
		 stmt.executeQuery(query);
		 stmt.close(); 
		
	 }
	 
	 /**
	  * Deletes habitat
	  * @param idAnimal
	  * @throws SQLException
	  */
	 public void deleteHabitat(int idAnimal) throws SQLException
	 {
		 OraclePreparedStatement pstmtDel = (OraclePreparedStatement) conn.prepareStatement("DELETE FROM habitat WHERE idAnimal=? ");
		 pstmtDel.setInt(1, idAnimal);
		 pstmtDel.execute();
		 pstmtDel.close();
	 }


	/**
	  * Gets the habitat area of a desired animal
	  * @param idAnimal id of animal
	  * @return	area in km^2
	  * @throws SQLException
	  */
	 public int getAnimalAreaSize(int idAnimal) throws SQLException
	{
		int area;
		Statement stmt=conn.createStatement();
		ResultSet rset=stmt.executeQuery("SELECT SUM(SDO_GEOM.SDO_AREA(geometry,1)) areaSize FROM habitat WHERE idAnimal="+idAnimal);
		while(rset.next())
		{
			area=rset.getInt(1);
			area=Math.round(area/3f);
			if (area != 0) return area; ///bulharska konstanta
			ResultSet rset2=stmt.executeQuery("SELECT SUM(SDO_GEOM.SDO_LENGTH(geometry,1)) areaSize FROM habitat WHERE idAnimal="+idAnimal);
			while(rset2.next())
			{
				return Math.round(rset2.getInt(1)/1.5f);
			}
		}
		return 0;
	}
	 
	
	/**
	 * Gets all animals orderd by area size from largest to smallest
	 * @return List of Animals ordered by area size
	 * @throws SQLException
	 */
	 public List<Animal> getAnimalsByArea() throws SQLException
	 {
		 Statement stmt = conn.createStatement();
		 ResultSet rset = stmt.executeQuery("SELECT an.idAnimal,an.genusname,an.speciesname,an.orderName,an.lawprotection,an.description, SUM(SDO_GEOM.SDO_AREA(h.geometry,0.1)) as area "
				+ "FROM animals an, habitat h "
				+ "WHERE an.idAnimal=h.idAnimal "
				+ "GROUP BY an.idAnimal,an.genusName,an.speciesName,an.orderName,an.lawprotection,an.description "
				+ "ORDER BY area DESC");
		 this.animals.clear();
		
		 while(rset.next())
		 {
			 this.animals.add(new Animal(rset.getInt(1), rset.getString(2), rset.getString(3),
					 rset.getString(4),getAnimalPopulation(rset.getInt(1)),rset.getBoolean(5),rset.getString(6)));
		 }
		 stmt.close();
		 rset.close();
		 return this.animals;
	 }
	 
	 
	 /**
	  * Gets animals that habitat intersects the habitat of the desired animal
	  * @param idAnimal id of animal
	  * @return List of Animals intersecting the habitat of the desired animal
	  * @throws SQLException
	  */
	 public List<Animal> getAnimalsSharingSameHabitat(int idAnimal) throws SQLException
	 {
		 OraclePreparedStatement pstmt1;
		 ResultSet rsetTmp;
		 OraclePreparedStatement pstmt2 = (OraclePreparedStatement) conn.prepareStatement("SELECT geometry FROM habitat WHERE idAnimal="+idAnimal);
		 ResultSet rset = pstmt2.executeQuery();
		 this.animals.clear();
		 while(rset.next())
		 {
			 pstmt1=(OraclePreparedStatement) conn.prepareStatement("SELECT an.idAnimal,an.genusName,an.speciesName,an.orderName,an.lawprotection,an.description "
			 		+ "FROM animals an, habitat h "
			 		+ "WHERE an.idAnimal=h.idAnimal AND h.idAnimal!="+idAnimal+" AND SDO_GEOM.SDO_INTERSECTION(h.geometry,?,1) IS NOT NULL");
			 pstmt1.setSTRUCT(1, ((OracleResultSet) rset).getSTRUCT("geometry"));
			 rsetTmp=(OracleResultSet) pstmt1.executeQuery();
			 while(rsetTmp.next())
			 {
				 this.animals.add(new Animal(rsetTmp.getInt(1),rsetTmp.getString(2),rsetTmp.getString(3),
						 rsetTmp.getString(4),getAnimalPopulation(rsetTmp.getInt(1)),rsetTmp.getBoolean(5),rsetTmp.getString(6)));
			 }
			 rsetTmp.close();
			 pstmt1.close();
		 }
		 rset.close();
		 
		 pstmt2.close();
		 return this.animals;
		 
	 }
	 
		/**
		 * Gets sum of populations at date
		 * @param date 
		 * @return population size of all monitored animals
		 * @throws SQLException
		 */
	 public int sumAnimalsAtDate(String date) throws SQLException
	 {
		 int sum;
		 sum = 0;
		 Statement stmt = conn.createStatement();
		 String sSum;
		 sSum = "SELECT SUM(POPULATION) FROM POPULATION WHERE ('"+date+"' >= VALIDFROM AND '"+date+"' < VALIDTO)";
		 ResultSet rset = stmt.executeQuery(sSum);
		 if(!(rset.next()))
		 {
			 return sum;
		 }
		 sum = rset.getInt(1);
		 rset.close();
		 stmt.close();
		 return sum;
	 }
	 
	
	 /**
		 * Delete population of one animal at date 
		 * @param idAnim id of animal
		 * @param date 
		 * @throws SQLException
		 */		
	 	  public void deletePop(int idAnim, String date) throws SQLException
		 {
			 Statement stmt = conn.createStatement();
			 String sDel;
			 sDel = "DELETE FROM POPULATION "
					+ "WHERE ('"+date+"' >= POPULATION.VALIDFROM AND '"+date+"' < POPULATION.VALIDTO) AND"
					+"(POPULATION.IDANIMAL = "+Integer.toString(idAnim)+")";
			 stmt.executeQuery(sDel);
			 stmt.close();
			 
		 }
		 
	 	 /**
			 * Add population to one animal from date to date 
			 * @param idAnim id of animal
			 * @param fromDate valid from date 
			 * @param toDate valid to date
			 * @param population population size  
			 * @throws SQLException
			 */		
		 public void addPop(int idAnim, String fromDate, String toDate, int population) throws SQLException
		 {
			int inIntervPopEq = 0;
			 Statement stmt = conn.createStatement();
			 String sQue;
			 sQue = "DELETE FROM POPULATION "
					+ "WHERE ('"+fromDate+"' <= POPULATION.VALIDFROM AND '"+toDate+"' >= POPULATION.VALIDTO) AND"
					+"(POPULATION.IDANIMAL = "+Integer.toString(idAnim)+")";
			 stmt.executeQuery(sQue);
			 
			 sQue = "SELECT idpop,validFrom,validTo,population FROM population "
		 		+ "WHERE ('"+fromDate+"' > POPULATION.VALIDFROM AND '"+toDate+"' < POPULATION.VALIDTO ) AND"
						+"(POPULATION.IDANIMAL = "+Integer.toString(idAnim)+")";
			 ResultSet rset=stmt.executeQuery(sQue);
			 
			 SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
			 Statement stmt2 = conn.createStatement();
			 if(rset.next())
			 {
				 
				 int tmpId=rset.getInt(1);
				 String tmpFromDate = format.format(rset.getDate(2));
				 String tmpToDate= format.format(rset.getDate(3));
				 int tmpPop=rset.getInt(4);
				 if(tmpPop!=population)
				 {
					sQue = "DELETE FROM POPULATION "
							+ "WHERE (POPULATION.IDPOP = "+Integer.toString(tmpId)+")";
					stmt2.executeQuery(sQue);
				 
					sQue ="INSERT INTO population(idAnimal,population,validFrom,validTo)"
						 +"VALUES("+Integer.toString(idAnim)+","+Integer.toString(tmpPop)+",'"+tmpFromDate+"','"+fromDate+"')";
					stmt2.executeQuery(sQue);
				 
					sQue ="INSERT INTO population(idAnimal,population,validFrom,validTo)"
						 +"VALUES("+Integer.toString(idAnim)+","+Integer.toString(tmpPop)+",'"+toDate+"','"+tmpToDate+"')";
					stmt2.executeQuery(sQue);
				}
				else
				{
					inIntervPopEq = 1;
				}
				 
			 }
			if(inIntervPopEq == 0)
			{
			 
				sQue = "UPDATE POPULATION "
					    +"SET VALIDFROM='"+toDate+"' "
						+ "WHERE ('"+fromDate+"' <= POPULATION.VALIDFROM AND '"+toDate+"' < POPULATION.VALIDTO AND '"+toDate+"' > POPULATION.VALIDFROM) AND"
						+"(POPULATION.IDANIMAL = "+Integer.toString(idAnim)+") AND (POPULATION.POPULATION<>"+Integer.toString(population)+")";
				stmt.executeQuery(sQue);
				 
				sQue = "UPDATE POPULATION "
						    +"SET VALIDTO='"+fromDate+"' "
							+ "WHERE ('"+fromDate+"' > POPULATION.VALIDFROM AND '"+fromDate+"' < POPULATION.VALIDTO AND '"+toDate+"' >= POPULATION.VALIDTO) AND"
							+"(POPULATION.IDANIMAL = "+Integer.toString(idAnim)+")  AND (POPULATION.POPULATION<>"+Integer.toString(population)+")";
				stmt.executeQuery(sQue);
				
				int fromBetween = 0;
				int fromBetweenId = 0;
				String fromBetweenFromDate = "";
				int toBetween = 0;
				int toBetweenId = 0;
				String toBetweenToDate = "";
				
				sQue = "SELECT idpop,validFrom,validTo,population FROM population "
						+ "WHERE ('"+fromDate+"' > POPULATION.VALIDFROM AND '"+fromDate+"' <= POPULATION.VALIDTO ) AND"
						+"(POPULATION.IDANIMAL = "+Integer.toString(idAnim)+") AND (POPULATION.POPULATION ="+Integer.toString(population)+")";
				ResultSet rset1=stmt.executeQuery(sQue);
				if(rset1.next())
				{
					fromBetween = 1;
					fromBetweenId=rset1.getInt(1);
					fromBetweenFromDate = format.format(rset1.getDate(2));
				
				}
				
				sQue = "SELECT idpop,validFrom,validTo,population FROM population "
						+ "WHERE ('"+toDate+"' >= POPULATION.VALIDFROM AND '"+toDate+"' < POPULATION.VALIDTO ) AND"
						+"(POPULATION.IDANIMAL = "+Integer.toString(idAnim)+") AND (POPULATION.POPULATION ="+Integer.toString(population)+")";
				ResultSet rset2=stmt.executeQuery(sQue);
				if(rset2.next())
				{
					toBetween = 1;
					toBetweenId=rset2.getInt(1);
					toBetweenToDate = format.format(rset2.getDate(3));
				}
				
				if((fromBetween == 1) && (toBetween == 1))
				{
					sQue = "DELETE FROM POPULATION "
							+ "WHERE (POPULATION.IDPOP = "+Integer.toString(fromBetweenId)+")";
					stmt.executeQuery(sQue);
					
					sQue = "DELETE FROM POPULATION "
							+ "WHERE (POPULATION.IDPOP = "+Integer.toString(toBetweenId)+")";
					stmt.executeQuery(sQue);
					
					sQue ="INSERT INTO population(idAnimal,population,validFrom,validTo)"
						+"VALUES("+Integer.toString(idAnim)+","+Integer.toString(population)+",'"+fromBetweenFromDate+"','"+toBetweenToDate+"')";
					stmt.executeQuery(sQue);
				
				
				}
				else if (fromBetween == 1)
				{
					sQue = "UPDATE POPULATION "
					    +"SET VALIDTO='"+toDate+"' "
						+ "WHERE (POPULATION.IDPOP = "+Integer.toString(fromBetweenId)+")";
					stmt.executeQuery(sQue);
				}
				else if (toBetween == 1)
				{
					sQue = "UPDATE POPULATION "
					    +"SET VALIDFROM='"+fromDate+"' "
						+ "WHERE (POPULATION.IDPOP = "+Integer.toString(toBetweenId)+")";
					stmt.executeQuery(sQue);
				}
				else
				{
					sQue ="INSERT INTO population(idAnimal,population,validFrom,validTo)"
						+"VALUES("+Integer.toString(idAnim)+","+Integer.toString(population)+",'"+fromDate+"','"+toDate+"')";
					stmt.executeQuery(sQue);
				}
				
			 
			 
				
			} 
			 stmt.close();
			 stmt2.close();
			
		 }
	 
	/**
	 * Searches all animals in databse that meet the search criteria	 
	 * @param animalSearchInfo map containing the search criterium and ints value
	 * @return List of found Animals 
	 * @throws SQLException
	 */
	public List<Animal> searchAnimals(Map<String, String> animalSearchInfo) throws SQLException
	{
		List<Integer> tmpId=new ArrayList<Integer>();
		tmpId.add(0);
		Statement stmt=conn.createStatement();
		String query="";
		
			query="SELECT ANIMALS.IDANIMAL, ANIMALS.GENUSNAME, ANIMALS.SPECIESNAME, ANIMALS.ORDERNAME, "
			 		+ "ANIMALS.LAWPROTECTION, POPULATION.POPULATION, ANIMALS.DESCRIPTION "
			 		+ "FROM ANIMALS "
					+ "LEFT JOIN POPULATION ON "
					+ "POPULATION.IDANIMAL=ANIMALS.IDANIMAL "
					+ "WHERE  ANIMALS.IDANIMAL!=1";
	
		
		this.animals.clear();
		for(Map.Entry<String, String>	entry : animalSearchInfo.entrySet()){
			if((entry.getKey().equals("lawProtection")) || (entry.getKey().equals("population.population > ")) || (entry.getKey().equals("population.population < ")))
			{
				query=query+" AND "+entry.getKey() +entry.getValue();
			}
			else query=query+" AND "+entry.getKey() + "'"+entry.getValue()+"'";
		}
		
		ResultSet rset=stmt.executeQuery(query);
		while(rset.next())
		{
			int match = 0;
			for (Iterator<Integer> ii = tmpId.iterator(); ii.hasNext();) 
			{
				if(rset.getInt(1)==ii.next()) match = 1;
			}
			if(match == 1) continue;
			this.animals.add(new Animal(rset.getInt(1),rset.getString(2),rset.getString(3),
					 rset.getString(4),getAnimalPopulation(rset.getInt(1)),rset.getBoolean(5),rset.getString(7)));
			tmpId.add(rset.getInt(1));
			
		}
	
		return this.animals;
	}
	 
	/**
	 * Closes the connection to database
	 * @throws SQLException
	 */
	 public void closeConnection() throws SQLException
	 {
		conn.close(); 
	 }
	 /**
	  * Gets the list iterator for Animals
	  * @return Iterator for List of Animals
	  */
	 public Iterator<Animal> getAnimalIterator() {
	        return this.animals.listIterator();
	    }
	 
	



}
